package preexercises.entites;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Worker extends Person {
    private String company;
    private String department;
    private List<String> projects;

    public Worker(String name, String surname, LocalDate birthday, String phoneNumber, String company, String
            department) {
        super(name, surname, birthday, phoneNumber);
        this.company = company;
        this.department = department;
        projects = new LinkedList<>();
    }

    @Override
    public String getFullInformation() {
        return "Worker: " + getCommonInformation() + company + " - " + department + ". Projects: " +
                projects.toString();
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setProjects(List<String> projects) {
        this.projects = projects;
    }

    public Worker addProject(String course) {
        projects.add(course);
        return this;
    }

    public List<String> getProjects() {
        return projects;
    }


}

