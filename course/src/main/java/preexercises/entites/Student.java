package preexercises.entites;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Student extends Person {
    private String university;
    private String specialization;
    private List<String> studiedCourses;

    public Student(String name, String surname, LocalDate birthday, String phoneNumber, String university, String
                    specialization) {
        super(name, surname, birthday, phoneNumber);
        this.university = university;
        this.specialization = specialization;
        studiedCourses = new LinkedList<>();
    }

    @Override
    public String getFullInformation() {
        return "Student: " + getCommonInformation() + university + " - " + specialization + ". Studied courses: " +
                studiedCourses.toString();
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Student addStudiedCourse(String course) {
        studiedCourses.add(course);
        return this;
    }

    public List<String> getStudiedCourses() {
        return studiedCourses;
    }


}

