package preexercises.entites;

import java.time.LocalDate;
import java.time.Period;

public abstract class Person {
    private String name;
    private String surname;
    private LocalDate birthday;
    private String phoneNumber;

    Person(String name, String surname, LocalDate birthday, String phoneNumber) {
        this.name = name.trim();
        this.surname = surname.trim();
        this.birthday = birthday;
        this.phoneNumber = phoneNumber.trim();
    }

    public String getFullName() {
        return (name + " " + surname).trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public int getAge() {
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public abstract String getFullInformation();

    protected String getCommonInformation(){
        return getFullName() + " (" + birthday.toString() + "). Phone number: " + phoneNumber + ". ";
    }


}
