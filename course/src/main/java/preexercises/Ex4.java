package preexercises;

import preexercises.entites.Person;
import preexercises.entites.Student;
import preexercises.entites.Worker;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Ex4 {
    public static void main(String...args){
        List<Person> persons = new ArrayList<>();

        Student student1 = new Student("John", "Smitt", LocalDate.of(1997, 12, 24),
                "+972542299675", "Big good university", "radio-tech");
        student1.addStudiedCourse("Programming").addStudiedCourse("mathematics").addStudiedCourse("history");

        Worker worker1 = new Worker("Yossi", "Kaz", LocalDate.of(1980, 1, 2),
                "0504859675", "AT&T", "Entertainment");
        worker1.addProject("Big data").addProject("Shaken");

        persons.add(student1);
        persons.add(worker1);


    persons.forEach(person -> System.out.println(person.getFullInformation()));

    }



}
