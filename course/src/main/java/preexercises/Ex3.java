package preexercises;

import java.util.HashMap;
import java.util.Map;

public class Ex3 {
    public static void main(String...args){
        Map<Double,Integer> numbers = new HashMap<>();
        for (String string : args) {
            try {
                double key = Double.valueOf(string);
                System.out.println(string + " - is convertable to " + key);
                int value = 0;
                if (numbers.containsKey(key))
                    value = numbers.get(key);
                numbers.put(key, value + 1);

            } catch (NumberFormatException e) {
                System.out.println(string + " - is not convertable");
            }
        }

        System.out.println(numbers);

    }
}
