package preexercises;

public class Ex2 {
    public static void main(String...args){
        double sum = 0.0;
        int count = 0;
        for (String string : args) {
            try {
                double value = Double.valueOf(string);
                System.out.println(value + " - is convertable");
                sum += value;
                count += 1;
                System.out.println("Sum=" + sum);
                System.out.println("Count=" + count);

            } catch (NumberFormatException e) {
                System.out.println(string + " - is not convertable");
            }
        }
        if (count == 0) {
            System.out.println("No arguments passed");
        } else {
            System.out.println("Average is: " + sum / count);
        }
    }
}
