package course.rectangles;

public class Rectangle {
    private final int x1;
    private final int x2;
    private final int y1;
    private final int y2;
    private final int area;
    private final int perimeter;

    Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = Math.min(x1, x2);
        this.x2 = Math.max(x1, x2);
        this.y1 = Math.min(y1, y2);
        this.y2 = Math.max(y1, y2);
        area = (this.x2 - this.x1) * (this.y2 - this.y1);
        perimeter = (this.x2 - this.x1) * 2 + (this.y2 - this.y1) * 2;
    }

    private int getArea() {
        return area;
    }

    private int getPerimeter() {
        return perimeter;
    }

    private int getX1() {
        return x1;
    }

    private int getX2() {
        return x2;
    }

    private int getY1() {
        return y1;
    }

    private int getY2() {
        return y2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle enemyRectangle = (Rectangle) o;
        return this.getPerimeter() == enemyRectangle.getPerimeter() && this.getArea() == enemyRectangle.getArea();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    boolean isContains(Rectangle enemyRectangle){
        return  (this.getX1() <= enemyRectangle.getX1() &&
                this.getX2() >= enemyRectangle.getX2() &&
                this.getY1() <= enemyRectangle.getY1() &&
                this.getY2() >= enemyRectangle.getY2());
    }

    String getMark(int i, int j, String symbol){
        String mark = " ";
        if ((i == y1 || i == y2) && (j == x1 || j == x2))
            mark = symbol;
        if ((i == y1 || i == y2) && (j > x1 && j < x2))
            mark = "-";
        if ((i > y1 && i < y2) && (j == x1 || j == x2))
            mark = "|";
        return mark;
    }

    boolean isMoreByAllParameters(Rectangle enemyRectangle){
        return this.getPerimeter() >= enemyRectangle.getPerimeter() && this.getArea() >= enemyRectangle.getArea();
    }

}
