package course.rectangles;

import java.util.Scanner;

public class Game {
    private final int maxX;
    private final int maxY;


    public Game(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
    }

    public void play(){
        Scanner scanner = new Scanner(System.in);
        int[] playerOneCoordinates = receiveCoordinatesFromCmd(scanner, "Player1");
        int[] playerTwoCoordinates = receiveCoordinatesFromCmd(scanner, "Player2");

        Rectangle player1 = new Rectangle(playerOneCoordinates[0], playerOneCoordinates[1], playerOneCoordinates[2], playerOneCoordinates[3]);
        Rectangle player2 = new Rectangle(playerTwoCoordinates[0], playerTwoCoordinates[1], playerTwoCoordinates[2], playerTwoCoordinates[3]);
        printRectangles(player1, player2);
        reportWinner(player1, player2);
    }

    private void printRectangles(Rectangle player1, Rectangle player2) {
        for (int i = 0; i <= maxY; i++) {
            for (int j = 0; j <= maxX; j++) {
                String player1Mark = player1.getMark(i,j,"1");
                String player2Mark = player2.getMark(i,j,"2");
                if (" ".equals(player1Mark) && !" ".equals(player2Mark))
                    System.out.print(player2Mark);
                else
                    System.out.print(player1Mark);
                System.out.print(" ");
            }
            System.out.print("\n");
        }
    }

    private void reportWinner(Rectangle player1, Rectangle player2) {
        String message;
        if (player1.equals(player2)) {
            message = "Tie!!! Have equals area and perimeter";
        } else if (player2.isContains(player1)) {
            message = "Player1 won!!!";
        } else if (player1.isContains(player2)) {
            message = "Player2 won!!!";
        } else if (player1.isMoreByAllParameters(player2)) {
            message = "Player1 won!!!";
        } else if (player2.isMoreByAllParameters(player1)) {
            message = "Player2 won!!!";
        } else {
            message = "Tie!!! ";
        }
        report(message);
    }

    private void report(String message){
        System.out.println(message);
    }

    private int[] receiveCoordinatesFromCmd(Scanner scanner, String name) {
        String coordinatesString;
        int[] parsedCoordinates = new int[4];
        do {
            System.out.println(name + ", enter coordinates between 0 and " + maxX + " (x), " + maxY + " (y), separated by whitespace (x1  y1  x2  y2): ");
            coordinatesString = scanner.nextLine();
        } while (!parseCoordinates(coordinatesString, parsedCoordinates));
        return parsedCoordinates;
    }

    private boolean parseCoordinates(String coordinates, int[] parsedCoordinates){
        String[] stringCoordinates = coordinates.trim().split("\\s+");
        if (stringCoordinates.length != 4) {
            System.out.println("Please enter exactly 4 (four) coordinates! Not less, not more!");
            return false;
        }
        for (int i = 0, coordinatesStringLength = stringCoordinates.length; i < coordinatesStringLength; i++) {
            String coordinate = stringCoordinates[i];
            int intValue;
            try {
                intValue = Integer.parseInt(coordinate);
            } catch (NumberFormatException e) {
                System.out.println("Please enter only integer numbers");
                return false;
            }
            if (intValue < 0) {
                System.out.println("Coordinate value should be more than 0");
                return false;
            }

            if ((i == 0 || i == 2) && intValue > maxX) {
                System.out.println("x coordinate should be less than field size (" + maxX + ")");
                return false;
            }
            if ((i == 1 || i == 3) && intValue > maxY) {
                System.out.println("y coordinate should be less than field size (" + maxY + ")");
                return false;
            }
            parsedCoordinates[i] = intValue;
        }
        return true;
    }
}
