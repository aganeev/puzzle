package course.files;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ATM implements Serializable {
    private String id;
    private Map<Integer,Integer> bills;

    public ATM(String id) {
        this.id = id;
        bills = new HashMap<>();
    }

    public void setBill(int bill, int amount){
        bills.put(bill, amount);
    }

    @Override
    public String toString() {
        return "ATM{" +
                "id='" + id + '\'' +
                ", bills=" + bills +
                '}';
    }
}
