package course.files;

import java.io.*;

public class ReCodeMachine {
    private static final int DEFAULT_BUFFER_SIZE = 4096;
    public static void main(String[] args) {
        String file1 = args[0];
        String file2 = args[2];
        String encoding1 = args[1];
        String encoding2 = args[3];

        char[] charBuffer = new char[DEFAULT_BUFFER_SIZE];
        try(InputStream is = new FileInputStream(new File(file1));
            InputStreamReader isr = new InputStreamReader(is, encoding1);
            OutputStream os = new FileOutputStream(new File(file2));
            OutputStreamWriter osw = new OutputStreamWriter(os, encoding2)){
            int n;
            while( (n = isr.read(charBuffer)) >= 0 ) {
                osw.write( charBuffer, 0, n );
            }
        }catch (IOException ex) {
            System.out.println(ex);
        }
    }

}
