package course.files;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

public class FilesMain {
    public static void main(String[] args) {
        String filename = "target\\myBinaryFile";

        ATM atm = new ATM("123");
        atm.setBill(20,30);
        atm.setBill(5, 1000);

        writeClassToFile(atm , filename);

        ATM atm2 = (ATM) readClassFromFile(filename);

        System.out.println(atm2);

//        int[] integersForFile = parseInts(args);
//
//        writeToFile(integersForFile, filename);

//        String data = readFromFile(filename);
//        System.out.println(data);

    }

    private static void writeClassToFile(Object obj, String filename) {
        try(OutputStream os = new FileOutputStream(new File(filename));
            ObjectOutputStream oos = new ObjectOutputStream(os)){
            oos.writeObject(obj);
        }catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static Object readClassFromFile(String filename) {
        Object obj = null;
        try(InputStream is = new FileInputStream(new File(filename));
            ObjectInputStream ois = new ObjectInputStream(is)){
            obj = ois.readObject();
        }catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
        }
        return obj;
    }

    private static void writeToFile(int[] integersForFile, String filename) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(integersForFile.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(integersForFile);
        try(OutputStream os = new FileOutputStream(new File(filename))){
            DataOutputStream dos = new DataOutputStream(os);
            dos.write(byteBuffer.array());
        }catch (IOException ex) {
            System.out.println(ex);
        }
    }


    private static int[] parseInts(String[] args) {
        List<Integer> parsedInts = new ArrayList<>();
        for (String arg : args) {
            try {
                Integer intArg = Integer.valueOf(arg);
                parsedInts.add(intArg);
            } catch (NumberFormatException e) {
                // ignore
            }
        }
        int[] valueToReturn = new int[parsedInts.size()];
        for (int i = 0; i < parsedInts.size(); i++) {
            valueToReturn[i] = parsedInts.get(i);
        }
        return valueToReturn;
    }
}
