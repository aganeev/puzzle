package course.files.customserialization;

import java.io.DataOutputStream;
import java.io.IOException;

public class Material {
    private MaterialName name;
    private int amount;

    public Material(MaterialName name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public void save(DataOutputStream outputStream) throws IOException {}
}
