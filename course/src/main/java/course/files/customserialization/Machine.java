package course.files.customserialization;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Machine {
    private String name;
    private Map<MaterialName, Integer> required;
    private Set<Material> loaded;
    private Map<Material, String> output;

    public Machine(String name, Map<MaterialName, Integer> required) {
        this.name = name;
        this.required = required;
        loaded = new HashSet<>();
        output = new HashMap<>();
    }

    public void addLoaded(Material material) {
        loaded.add(material);
    }

    public void addOutput(Material material, String machine) {
        output.put(material, machine);
    }

    public void save(DataOutputStream outputStream) throws IOException {}
}
