package course.files.customserialization;

import java.util.HashMap;
import java.util.Map;

public class MainFactory {
    public static void main(String[] args) {

        Map<MaterialName, Integer> machineArequirements = new HashMap<>();
        machineArequirements.put(MaterialName.A, 25);
        machineArequirements.put(MaterialName.B, 5);

        Machine machineA = new Machine("machineA", machineArequirements);

        machineA.addLoaded(new Material(MaterialName.A, 20));
        machineA.addOutput(new Material(MaterialName.B, 10), "machineB");


        Map<MaterialName, Integer> machineBrequirements = new HashMap<>();
        machineArequirements.put(MaterialName.C, 10);
        machineArequirements.put(MaterialName.D, 100);

        Machine machineB = new Machine("machineB", machineBrequirements);

        machineB.addLoaded(new Material(MaterialName.C, 30));
        machineB.addOutput(new Material(MaterialName.D, 5), "machineA");


        Factory factory = new Factory("my Factory");
        factory.addMachine(machineA);
        factory.addMachine(machineB);
        factory.addProduct(new Material(MaterialName.A, 20));
        factory.addProduct(new Material(MaterialName.D, 90));



    }
}
