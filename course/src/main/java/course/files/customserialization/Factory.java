package course.files.customserialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Factory {
    private String name;
    private Set<Machine> machines;
    private Set<Material> pruducedForSale;


    public Factory(String name) {
        this.name = name;
        machines = new HashSet<>();
        pruducedForSale = new HashSet<>();
    }

    public void addMachine(Machine machine){
        machines.add(machine);
    }

    public void addProduct(Material material){
        pruducedForSale.add(material);
    }


    public void save(DataOutputStream outputStream) throws IOException {
        outputStream.writeUTF(name);
        outputStream.writeInt(machines.size());
        for (Machine machine : machines) {
            machine.save(outputStream);
        }
        outputStream.writeInt(pruducedForSale.size());
        for (Material product : pruducedForSale) {
            product.save(outputStream);
        }
    }

    public static Factory load(DataInputStream inputStream) throws IOException {
        String name = inputStream.readUTF();
        Factory factory = new Factory(name);
        int machinesNumber = inputStream.readInt();
        for (int i = 0 ; i < machinesNumber; i++) {
//            factory.addMachine(Machine.load(inputStream));
        }
        int productNumbers = inputStream.readInt();
        for (int i = 0 ; i < productNumbers; i++) {
//            factory.addProduct(Material.load(inputStream));
        }
        return factory;
    }
}
