package course.java8;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class J8Main {
    public static void main(String[] args) throws IOException {
        List<Person> family = new ArrayList<>();

        family.add(new Person("John", 30, Gender.MALE));
        family.add(new Person("Jane", 20, Gender.FRMALE));
        family.add(new Person("Abram", 15, Gender.MALE));
        family.add(new Person("Judy", 35, Gender.FRMALE));
        family.add(new Person("Bill", 30, Gender.MALE));

        // 1 - Lambda
//        Collections.sort(family, (person1,person2) -> person1.getAge() - person2.getAge());
//
//        family.forEach(System.out::println);
//        System.out.println();
//
//        Collections.sort(family, (person1,person2) -> person1.getName().compareTo(person2.getName()));
//
//        family.forEach(System.out::println);


        //2 - Stream
        // average
//        int average = (int) family.stream().mapToInt(Person::getAge).average().getAsDouble();
//        System.out.println(average);
//
//        System.out.println();
//        //  older than average
//        family.stream().filter(person -> person.getAge() > average).forEach(System.out::println);
//
//        System.out.println();
//
//        // females
//        family.stream().filter(person -> person.getGender().equals(Gender.FRMALE)).forEach(System.out::println);
//
//        System.out.println();
//        // sorted by name
//        List<Person> sortedFamily = family.stream().sorted(Comparator.comparing(Person::getName)).collect(Collectors.toList());
//
//        sortedFamily.forEach(System.out::println);


        // Files
        Path path = Paths.get("C:/");

        System.out.println(path.toAbsolutePath());

//        System.out.println(Files.walk(path.toAbsolutePath()).map(path1 -> path1.).filter(file->file.getFileName().endsWith(".txt"))
//                .findFirst());



//                .filter(file->file.getFileName().endsWith(".java"))
//                .forEach(System.out::println);
//                .map(Path::toFile)
//                .mapToLong(File::length)
//                .sum());
    }
}
