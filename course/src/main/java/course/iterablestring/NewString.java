package course.iterablestring;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class NewString implements Iterable<Character> {
    private String string;

    public NewString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public Iterator<Character> iterator() {
        return new Iterator<Character>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < string.length();
            }

            @Override
            public Character next() {
                try {
                    return string.charAt(currentIndex++);
                } catch (IndexOutOfBoundsException ex) {
                    throw new NoSuchElementException(ex.getMessage());
                }
            }

        };
    }
}
