package course.binarytree;

public class BinaryTreeGeneric<E extends Comparable<E>> {
    private TreeNode head;

    public void add(E newValue){
        if (head == null)
            head = new TreeNode(newValue);
        else
            head.add(newValue);
    }

    public void print() {
        if (head != null)
            head.print();
    }


    private class TreeNode<E extends Comparable<E>> {
        private E value;
        private TreeNode[] children;

        private TreeNode(E value) {
            this.value = value;
            children = new TreeNode[2];
        }

        private void add(E newValue){
            if (newValue.compareTo(value) <= 0) {
                addHandleNull(newValue, 0);
            } else {
                addHandleNull(newValue, 1);
            }
        }

        private void addHandleNull(E newValue, int index) {
            if (children[index] == null)
                children[index] = new TreeNode(newValue);
            else
                children[index].add(newValue);
        }

        private void print(){
            if (children[0] != null)
                children[0].print();
            System.out.println(value);
            if (children[1] != null)
                children[1].print();
        }

    }





}
