package course.binarytree;

public class BinaryTree<E extends Comparable<E>> {
    private TreeNode head;

    public void add(E newValue){
        if (head == null) {
            head = new TreeNode<E>(newValue);
        }
        else {
            head.add(newValue);
        }
    }

    public void print() {
        if (head != null) {
            head.print();
        }
        else {
            System.out.println("Tree is empty");
        }
    }

    public void printAsTree() {
        if (head != null) {
            head.printAsTree(0);
        }
        else {
            System.out.println("Tree is empty");
        }
    }

    private enum Leaf {
        LEFT(0),
        RIGHT(1);

        private int index;
        Leaf(int index) {
            this.index = index;
        }
        public int index() {
            return index;
        }
    };

    private class TreeNode<E extends Comparable<E>> {
        private E value;
        private TreeNode[] children;

        private TreeNode(E value) {
            this.value = value;
            children = new TreeNode[2];
        }

        private void add(E newValue){
            if (newValue.compareTo(value) < 0) {
                addLeft(newValue);
            } else {
                addRight(newValue);
            }
        }

        private void addLeft(E newValue) {
            addHandleNull(newValue, Leaf.LEFT);
        }

        private void addRight(E newValue) {
            addHandleNull(newValue, Leaf.RIGHT);
        }

        private void addHandleNull(E newValue, Leaf leaf) {
            if (children[leaf.index()] == null) {
                children[leaf.index()] = new TreeNode(newValue);
            }
            else {
                children[leaf.index()].add(newValue);
            }
        }

        private void printAsTree(int depth) {
            print(Leaf.LEFT, depth + 1);
            // print self value
            for(int i=0; i<depth; ++i) {
                System.out.print("\t");
            }
            System.out.println(value);
            print(Leaf.RIGHT, depth + 1);
        }

        private void print() {
            print(Leaf.LEFT);
            // print self value
            System.out.println(value);
            print(Leaf.RIGHT);
        }

        private void print(Leaf leaf) {
            if (children[leaf.index()] != null) {
                children[leaf.index()].print();
            }
        }

        private void print(Leaf leaf, int depth) {
            if (children[leaf.index()] != null) {
                children[leaf.index()].printAsTree(depth);
            }
        }
    }





}
