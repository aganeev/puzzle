package course.binarytree;

import java.util.Random;

public class BinaryTreeMain {
    public static void main(String[] args) {
        BinaryTree<Integer> tree = new BinaryTree<>();
        Random random = new Random();
        for (int i = 0 ; i < 8; i++) {
            tree.add(random.nextInt(200));
        }
        // tree.print();
        tree.printAsTree();

        BinaryTree<String> treeOfStrings = new BinaryTree<>();
        for (int i = 0 ; i < 8; i++) {
            treeOfStrings.add(randomAlphaNumeric(6));
        }
        // tree.print();
        treeOfStrings.printAsTree();


    }


    public static String randomAlphaNumeric(int count) {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
}
