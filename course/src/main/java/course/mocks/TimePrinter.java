package course.mocks;

public class TimePrinter {
    private final Timer1 timer;

    public TimePrinter(Timer1 timer) {
        this.timer = timer;
    }

    public String getPrettyTime() {
        return "Time: " + this.timer.getTime();
    }
}
