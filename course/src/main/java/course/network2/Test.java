package course.network2;

public class Test {

    private enum FREQUENCY {GHZ, MHZ};
    private enum CAMERA {A,B,C};

    private CAMERA camera;
    private FREQUENCY frequency;

    public Test(CAMERA camera, FREQUENCY frequency) {
        this.camera = camera;
        this.frequency = frequency;
    }

    public void setCamera(CAMERA camera) {
        this.camera = camera;
    }

    public void setFrequency(FREQUENCY frequency) {
        this.frequency = frequency;
    }
}
