//package course.deeprecursion;
//
//import java.io.*;
//
//public class TooDeepRecursionMain {
//    public static void main(String[] args) {
//        try(InputStream is = new FileInputStream(new File(args[0]));
//            InputStreamReader isr = new InputStreamReader(is)) {
//            printCharsFromInputStream(isr);
//        }catch (IOException e) {
//            System.out.println(e);
//        }
//    }
//
//// For src\main\resources\all_1.log file it works with -Xss2m and doesn't work with -Xss1m
//
//
//
//
//    private static void printCharsFromInputStream(InputStreamReader inputStreamReader) {
//        try(inputStreamReader){
//            int intChar = inputStreamReader.read();
//            if (intChar == -1) {
//                return;
//            }
//            System.out.print((char)intChar);
//            printCharsFromInputStream(inputStreamReader);
//        }catch (IOException e) {
//            System.out.println(e);
//        }
//    }
//}
