package course.performance;

public class BenchmarkFix2 {
    private String test() {
        String result = "";
        for(char i = '0'; i <= '9'; ++i) {
            result += i;
        }
        return result;
    }

    public static void main (String[] args) throws Exception {
        long start = System.nanoTime();
        for(int i = 0; i < 1000000; ++i) {
            new BenchmarkFix2().test();
        }
        System.out.println(System.nanoTime() - start);
    }
}
