package course.performance;


class Benchmark {
    static private void test1() throws RuntimeException {
    }

    static private void test2() {
    }

    static void benchmark(Runnable method) {
        long start = System.nanoTime();
        for(int i = 0; i < 1_000_000; ++i) {
            method.run();
        }
        System.out.println("elapsed time (ns): " + (System.nanoTime() - start));
    }
    public static void main (String[] args) {
        Benchmark.benchmark(Benchmark::test1);
        Benchmark.benchmark(Benchmark::test2);
        Benchmark.benchmark(Benchmark::test2);
        Benchmark.benchmark(Benchmark::test1);
        Benchmark.benchmark(Benchmark::test2);
        Benchmark.benchmark(Benchmark::test1);
        Benchmark.benchmark(Benchmark::test2);
        Benchmark.benchmark(Benchmark::test1);
    }
}

