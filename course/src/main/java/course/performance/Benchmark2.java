package course.performance;


class Benchmark2 {
    static private String singleReturn() {
        return "1" + 2;
    }

    static private String singleException() throws IllegalArgumentException {
        throw new IllegalArgumentException("12");
    }

    static private String recursiveReturn(int count) {
        if (count < 1000) {
            return recursiveReturn(++count);
        } else {
            return "1" + 2;
        }
    }

    static private String recursiveException(int count) throws IllegalArgumentException {
        if (count < 1000) {
                return recursiveException(++count);
        } else {
            throw new IllegalArgumentException("12");
        }
    }


    static void benchmark(Runnable method) {
        long start = System.nanoTime();
        for(int i = 0; i < 1_000_000; ++i) {
            try {
                method.run();
            } catch (IllegalArgumentException e) {
                // Ignore
            }
        }
        System.out.println("elapsed time (ns): " + (System.nanoTime() - start));
    }
    public static void main (String[] args) {
        Benchmark2.benchmark(Benchmark2::singleReturn);
        Benchmark2.benchmark(Benchmark2::singleException);
        Benchmark2.benchmark(()->recursiveReturn(0));
        Benchmark2.benchmark(()->recursiveException(0));
        System.out.println(" ");
        Benchmark2.benchmark(()->recursiveException(0));
        Benchmark2.benchmark(()->recursiveReturn(0));
        Benchmark2.benchmark(Benchmark2::singleException);
        Benchmark2.benchmark(Benchmark2::singleReturn);



    }
}

