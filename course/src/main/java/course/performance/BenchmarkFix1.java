package course.performance;

public class BenchmarkFix1 {
    private String test() {
        StringBuilder result = new StringBuilder();
        for(char i = 0; i < 10; ++i) {
            result.append(i);
        }
        return result.toString();
    }

    public static void main (String[] args) throws Exception {
        long start = System.nanoTime();
        for(int i = 0; i < 1000000; ++i) {
            new BenchmarkFix1().test();
        }
        System.out.println(System.nanoTime() - start);
    }
}
