package course.performance.stringconversion;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class StringConvertor {


    public static int readFromFile(String path) {
        int sum = 0;
        try (FileReader fileReader = new FileReader(new File(path));
             Scanner scanner = new Scanner(fileReader)) {
            while (scanner.hasNext()) {
                String stringNum = scanner.next();
                sum += parseWithTry(stringNum);
            }
        } catch (IOException ex) {
            // Ignore
        }
        return sum;
    }

    public static int readFromFileBetter(String path) {
        int sum = 0;
        try (FileReader fileReader = new FileReader(new File(path));
             Scanner scanner = new Scanner(fileReader)) {
            while (scanner.hasNext("\\d+")) {
                String stringNum = scanner.next("\\d+");
                sum += parseWithTry(stringNum);
            }
        } catch (IOException ex) {
            // Ignore
        }
        return sum;
    }

    public static int readFromFileTheBest(String path) {
        int sum = 0;
        try (FileReader fileReader = new FileReader(new File(path));
             Scanner scanner = new Scanner(fileReader)) {
            while (scanner.hasNext()) {
                String stringNum = scanner.next();
                sum += myParse(stringNum);
            }
        } catch (IOException ex) {
            // Ignore
        }
        return sum;
    }

    private static int myParse(String stringNum) {
        char[] chars = stringNum.toCharArray();
        int[] ints = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            int intt = (int) chars[i];
//            if (ch)
//            ints[i] = (int) ;
        }
        return 0;
    }

    private static int parseWithTry(String stringNum) {
        int num = 0;
        try {
            num = Integer.parseInt(stringNum);
        } catch (NumberFormatException ex) {
            // Ignore
        }
        return num;
    }

    static void benchmark(Consumer<String> method, String param) {
        long start = System.nanoTime();
        for(int i = 0; i < 10_000; ++i) {
            method.accept(param);
        }
        System.out.println("elapsed time (ns): " + (System.nanoTime() - start));
    }

    public static void main(String[] args) {
//        System.out.println("Warming..");
//        StringConvertor.benchmark(StringConvertor::readFromFile, "src/main/resources/numbers");
//        StringConvertor.benchmark(StringConvertor::readFromFileBetter, "src/main/resources/numbers");
//        System.out.println("readFromFile");
//        StringConvertor.benchmark(StringConvertor::readFromFile, "src/main/resources/numbers");
//        System.out.println("readFromFileBetter");
//        StringConvertor.benchmark(StringConvertor::readFromFileBetter, "src/main/resources/numbers");
//        StringConvertor.benchmark(StringConvertor::readFromFileBetter, "src/main/resources/numbers");
//        System.out.println("readFromFile");
//        StringConvertor.benchmark(StringConvertor::readFromFile, "src/main/resources/numbers");

        int intt = (int) '/';
        int intt2 = '/';
        int intt3 = (int) '0';
        int intt4 = '0';
        System.out.println(intt + " " + intt2 + " " + intt3 + " " + intt4);
    }
}
