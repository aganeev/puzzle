package course.performance.jmh;

import java.io.Serializable;
import java.util.Random;

public class Int2DArrayStatic implements Serializable {
    private static final int[][] myArray = new int[100][100];

    static {
        Random r = new Random();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                myArray[i][j] = r.nextInt(99) + 1;
            }
        }
        System.out.println("Initialized");
    }

    public static void printArray(){
        for (int i = 0; i < 5 ; i++) {
            System.out.println(myArray[0][i]);
        }
    }

    public static long sumArrayRowsColumns(){
        long sum = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                sum += myArray[i][j];
            }
        }
        return sum;
    }

    public static long sumArrayColumnsRows(){
        long sum = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                sum += myArray[j][i];
            }
        }
        return sum;
    }
}
