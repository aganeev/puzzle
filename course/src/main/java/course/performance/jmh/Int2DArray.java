package course.performance.jmh;

import java.io.Serializable;
import java.util.Random;

public class Int2DArray implements Serializable {
    private int[][] myArray = new int[100][100];

    public Int2DArray() {
        Random r = new Random();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                myArray[i][j] = r.nextInt(99) + 1;
            }
        }
        System.out.println("Initialized");
    }

    public void printArray(){
        for (int i = 0; i < 5 ; i++) {
            System.out.println(myArray[0][i]);
        }
    }

    public long sumArrayRowsColumns(){
        long sum = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                sum += myArray[i][j];
            }
        }
        return sum;
    }

    public long sumArrayColumnsRows(){
        long sum = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                sum += myArray[j][i];
            }
        }
        return sum;
    }
}
