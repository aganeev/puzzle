package course.performance.jmh;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.*;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.All)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 2)
@Measurement(iterations = 2, time = 1)
@Fork(2)
public class Int2DArrayBenchmark {
    Int2DArray my2DArray;

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Int2DArrayBenchmark.class.getSimpleName())
                .build();
        Int2DArray myArray = new Int2DArray();
        writeClassToFile(myArray, "src/main/resources/2dIntArray");
        new Runner(opt).run();
    }

    @Setup(Level.Iteration)
    public void loadFromFile(){
        my2DArray = (Int2DArray) readClassFromFile("src/main/resources/2dIntArray");
    }

    @Benchmark
    public void measureSumRowsColumns(Blackhole blackhole) {
        long sum = my2DArray.sumArrayRowsColumns();
        blackhole.consume(sum);
    }

    @Benchmark
    public void measureSumColumnsRows(Blackhole blackhole) {
        long sum = my2DArray.sumArrayColumnsRows();
        blackhole.consume(sum);
    }

    private static void writeClassToFile(Object obj, String filename) {
        try(OutputStream os = new FileOutputStream(new File(filename));
            ObjectOutputStream oos = new ObjectOutputStream(os)){
            oos.writeObject(obj);
        }catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static Object readClassFromFile(String filename) {
        Object obj = null;
        try(InputStream is = new FileInputStream(new File(filename));
            ObjectInputStream ois = new ObjectInputStream(is)){
            obj = ois.readObject();
        }catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
        }
        return obj;
    }

}
