package course.network;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {
    private boolean isFinished;

    public static void main(String[] args) {
        ChatClient client = new ChatClient();
        client.start();
    }


    public void start() {

        try (Socket socket = new Socket("localhost", 7000);
             PrintStream socketOutput = new PrintStream(socket.getOutputStream(),true, "UTF8");
             Scanner scanner = new Scanner(System.in);
             BufferedReader socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF8")))
        {

            // Thread for input
            new Thread(()->{
                try {
                    String serverLine = "";
                    while(serverLine != null && !serverLine.equals("null")) {
                        System.out.println(serverLine);
                        serverLine = socketInput.readLine();
                    }

                } catch (IOException ex) {
                    // Ignore
                } finally {
                    System.out.println("Your session has closed.");
                    isFinished = true;
                    System.exit(0);
                }
            }).start();

            while(!isFinished) {
                String line = scanner.nextLine();
                socketOutput.println(line);
            }

        } catch (IOException e) {
            // Ignore
        } catch (IllegalStateException ise) {
            System.out.println("Connection lost");
        }
    }

}
