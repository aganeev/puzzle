package course.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Server {
    private static final String YOU_HAVE_BEEN_EXPELLED = "You have been expelled by %s";
    private List<ClientHandler> clientList;
    private ServerSocket server;

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.init();
    }

    private void init() throws IOException {
        clientList = new ArrayList<>();
        try (ServerSocket server = new ServerSocket(7000)) {
            this.server = server;
            int clientNum = 1;
            while (!server.isClosed()) {
                ClientHandler handler = new ClientHandler(this, server.accept(), clientNum++);
                Thread thread = new Thread(handler);
                synchronized (this) {
                    clientList.add(handler);
                    thread.start();
                }
            }
        } catch (SocketException ex) {
            System.out.println("Got Shutdown command. Shutting down...");
        }
    }

    void shutdown() {
        synchronized (this) {
            for (ClientHandler client : clientList) {
                client.close();
            }
            clientList.clear();
        }
        if (server != null && !server.isClosed()) {
            try {
                server.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    void unregister(ClientHandler client) {
        synchronized (this) {
            clientList.remove(client);
        }
    }

    void sentToAll(int clientNum, String message) {
        synchronized (this) {
            clientList.forEach(clientHandler -> clientHandler.sendToClient(clientNum, message));
        }
    }

    void kill(String nick, int killerId, String killerNick) {
        synchronized (this) {
            ClientHandler killed = clientList.stream()
                    .filter(client -> client.getNick().equals(nick))
                    .findFirst()
                    .orElse(null);
            if (killed != null) {
                killed.sendToClient(killerId, String.format(YOU_HAVE_BEEN_EXPELLED, killerNick));
                killed.close();
                unregister(killed);
                sentToAll(0, String.format("%s has quited from the chat", nick));
                clientList.remove(killed);
            }
        }
    }

    void killAll(int killerId, String killerNick) {
        synchronized (this) {
            List<ClientHandler> toRemove = clientList.stream()
                    .filter(client -> client.getClientNum() != killerId)
                    .peek(client -> {
                        client.sendToClient(killerId, String.format(YOU_HAVE_BEEN_EXPELLED, killerNick));
                        client.close();
                    }).collect(Collectors.toList());
            toRemove.forEach(clientList::remove);
        }
    }

    boolean isNickUsed(String nick) {
        synchronized (this) {
            return clientList.stream()
                    .anyMatch(client -> client.getNick().equals(nick));
        }
    }
}

