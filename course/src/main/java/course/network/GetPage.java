package course.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetPage {
    public static void main(String[] args) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://example.com");
            // TODO: set connection and read timeouts on the connection
            conn = (HttpURLConnection)url.openConnection();
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF8"))) {
                conn.getHeaderFields().entrySet().forEach(System.out::println);
                System.out.println();
                String line = "";
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            }
        } catch (Exception ex) {
            // TODO: write to log
        } finally {
            if (conn != null) {
                conn.disconnect(); // unfortunately it is not AutoClosable...
            }
        }
    }
}
