package course.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ClientHandler extends Thread {
    private static final String KILL_ALL = "kill\\s+them\\s+all";
    private static final String CLOSE_CHAT = "shutdown\\s+now";
    private static final String KILL = "^kill\\s+(.*)";
    private static final String QUIT = "quit";
    private static final String CHANGE_NICK = "^new_nick=(.*)";
    private Socket socket;
    private Server server;
    private int clientNum;
    private PrintStream clientOutput;
    private String nick;
    private boolean isAdmin;
    private boolean isClosedByServer;

    ClientHandler(Server server, Socket socket, int clientNum) {
        this.socket = socket;
        this.server = server;
        this.clientNum = clientNum;
        nick = String.valueOf(clientNum);
    }

    @Override
    public void run() {
        try (BufferedReader clientInput = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF8"));
             PrintStream clientOutput = new PrintStream(socket.getOutputStream(), true, "UTF8")) {
            this.clientOutput = clientOutput;
            System.out.printf("ChatClient %s is connected%n", clientNum);
            clientOutput.write(clientNum);
            clientOutput.flush();
            clientOutput.println("Welcome to our chat");
            clientOutput.println(String.format("Your nick is %s. You can change it by the command 'new_nick=<new nick>'",clientNum));
            String message = String.format("%s has came to the chat room", nick);
            while (!socket.isClosed()) {
                if (!"".equals(message)) {server.sentToAll(clientNum, message);}
                System.out.println(message);
                String line = clientInput.readLine().trim();
                message = handleSpecialCommands(line);
            }

        } catch (IOException ex) {
            System.out.printf("[%s] Session closed%n", clientNum);
        } finally {
            try {
                socket.close();
                if (!isClosedByServer) {server.unregister(this);}
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    private String handleSpecialCommands(String command) { //TODO change to boolean
        if (command.matches(CHANGE_NICK)) {
            return handleNewNick(command);
        }
        if (command.toLowerCase().equals(QUIT)) {
            System.out.printf("[%s] disconnected%n", clientNum);
            server.kill(nick, clientNum, nick);
            return "";
        }
        if (isAdmin) {
            if (command.matches(CLOSE_CHAT)) {
                clientOutput.printf("%s, you have stopped the server%n", nick);
                server.shutdown();
                return "";
            }
            if (command.matches(KILL_ALL)) {
                clientOutput.printf("%s, you have expelled all other participants%n", nick);
                server.killAll(clientNum, nick);
                return "";
            }
            if (command.matches(KILL)) {
                String expelledNick = command.replaceFirst("kill\\s", "");
                clientOutput.printf("%s, you have expelled %s%n", nick, expelledNick);
                server.kill(expelledNick, clientNum, nick);
                return "";
            }
        }
        return String.format("[%s] %s", nick, command);
    }


    private String handleNewNick(String line) {
        int index = line.indexOf('=');
        String newNick = line.substring(index + 1);
        if (server.isNickUsed(newNick)) {
            clientOutput.printf("Nick %s is already used%n", newNick);
            return String.format("[%s] I haven't enough imagination to come up with an original nickname", nick);
        }
        String oldNick = nick;
        nick = newNick;
        if (nick.equals("Admin")) {
            isAdmin = true;
        }
        clientOutput.printf("Your nick is changed to %s%n", nick);
        return String.format("%s has changed nick to %s", oldNick, nick);
    }


    public void close() {
        isClosedByServer = true;
        if (socket != null && !socket.isClosed()) {
            try {
                clientOutput.print((Object) null);
                clientOutput.flush();
                socket.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    void sendToClient(int clientNum, String line) {
        if (this.clientNum != clientNum && clientOutput != null) {
            try {
                clientOutput.println(line);
            } catch (Exception e) {
                // Ignore
            }
        }
    }

    String getNick() {
        return nick;
    }

    int getClientNum() {
        return clientNum;
    }
}