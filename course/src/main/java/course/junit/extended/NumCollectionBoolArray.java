//package course.junit.extended;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//
//public class NumCollectionBoolArray extends NumCollectionAbstract {
//    private boolean[] collection;
//    private int smallest;
//
//
//    NumCollectionBoolArray(ArrayList<int[]> pairs) {
//        smallest = pairs.get(0)[MIN];
//        collection = new boolean[pairs.get(pairs.size() - 1)[MAX] - smallest + 1];
//        for (int[] pair : pairs) {
//            for (int j = pair[MIN] - this.smallest; j <= pair[MAX] - this.smallest; j++) {
//                collection[j] = true;
//            }
//        }
//    }
//
//    @Override
//    boolean contains(int number){
//        int index = number - smallest;
//        if (index < 0 || index > collection.length - 1) {
//            return false;
//        }
//        return collection[index];
//    }
//
//    @Override
//    public Iterator<Integer> iterator() {
//        return new Iterator<>() {
//            int currentValue = smallest - 1;
//
//            @Override
//            public boolean hasNext() {
//                return (currentValue < collection.length + smallest);
//            }
//
//            @Override
//            public Integer next() {
//                do {
//                    currentValue++;
//                } while (!collection[currentValue - smallest]);
//                return currentValue;
//            }
//        };
//    }
//
//
//
//}
