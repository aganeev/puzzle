package course.junit.extended;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class NumCollectionIntSet extends NumCollectionAbstract {
    private Set<Integer> collection;

    NumCollectionIntSet(ArrayList<int[]> pairs) {
        collection = new HashSet<>();
        for (int[] pair : pairs) {
            for (int i = pair[MIN]; i <= pair[MAX]; i++) {
                collection.add(i);
            }
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return collection.iterator();
    }

    @Override
    boolean contains(int i) {
        return collection.contains(i);
    }
}
