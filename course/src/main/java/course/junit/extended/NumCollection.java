//package course.junit.extended;
//
//import course.junit.UnsortedValuesException;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//
//public class NumCollection implements Iterable<Integer>{
//    private NumCollectionAbstract collection;
//    private static int minMaxThreshold = 1000;
//    private static int pairsNumThreshold = 100;
//
//    private static final int MIN = 0;
//    private static final int MAX = 1;
//
//
//    public NumCollection(String numCollectionString) throws UnsortedValuesException {
//        String[] separatedByComma = numCollectionString.split(",");
//        int smallest = parseValues(separatedByComma[0])[MIN];
//        int largest = parseValues(separatedByComma[separatedByComma.length - 1])[MAX];
//        if (smallest > largest) {
//            throw new UnsortedValuesException();
//        }
//        int largestMin = smallest;
//        ArrayList<int[]> pairs = new ArrayList<>(separatedByComma.length);
//        for (String aSeparatedByComma : separatedByComma) {
//            int[] parsedValues = parseValues(aSeparatedByComma);
//            if (parsedValues[MIN] < largestMin || parsedValues[MAX] > largest) {
//                throw new UnsortedValuesException();
//            } else {
//                pairs.add(parsedValues);
//                largestMin = parsedValues[MAX];
//            }
//        }
//
//
//        if (largest - smallest + 1 <= minMaxThreshold) {
//            collection = new NumCollectionBoolArray(pairs);
//        } else {
//            if (pairs.size() <= pairsNumThreshold) {
//                collection = new NumCollectionIntArray(pairs);
//            } else {
//                collection = new NumCollectionIntSet(pairs);
//            }
//        }
//    }
//
//    private static int[] parseValues(String value) throws UnsortedValuesException {
//        int[] returnValue = new int[2];
//        value = value.trim();
//        int dashIndex = value.indexOf('-', 1);
//        if (dashIndex >= 0) {
//            int firstInt = Integer.parseInt(value.substring(0, dashIndex));
//            int secondInt = Integer.parseInt(value.substring(dashIndex + 1));
//            if (firstInt > secondInt) {
//                throw new UnsortedValuesException();
//            }
//            returnValue[0] = firstInt;
//            returnValue[1] = secondInt;
//        } else {
//            int intValue = Integer.parseInt(value);
//            returnValue[0] = returnValue[1] = intValue;
//        }
//        return returnValue;
//    }
//
//
//
//    public boolean contains(int number){
//        return collection.contains(number);
//    }
//
//    @Override
//    public Iterator<Integer> iterator() {
//        return collection.iterator();
//    }
//
//    Class getCollectionClass(){
//        return collection.getClass();
//    }
//
//    static void setMinMaxThreshold(int minMaxThreshold) {
//        NumCollection.minMaxThreshold = minMaxThreshold;
//    }
//
//    static void setPairsNumThreshold(int pairsNumThreshold) {
//        NumCollection.pairsNumThreshold = pairsNumThreshold;
//    }
//}
