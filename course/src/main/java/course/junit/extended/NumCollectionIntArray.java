//package course.junit.extended;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//
//public class NumCollectionIntArray extends NumCollectionAbstract {
//    private ArrayList<int[]> collection;
//
//    NumCollectionIntArray(ArrayList<int[]> parsedValues) {
//        collection = parsedValues;
//    }
//
//    @Override
//    public Iterator<Integer> iterator() {
//        return new Iterator<>() {
//            Iterator<int[]> parentIterator = collection.iterator();
//            int[] currentPair = parentIterator.next();
//            int currentValue = currentPair[MIN] - 1;
//
//            @Override
//            public boolean hasNext() {
//                return (parentIterator.hasNext() || currentValue < currentPair[MAX]);
//            }
//
//            @Override
//            public Integer next() {
//                if (currentValue < currentPair[MAX]) {
//                    currentValue++;
//                } else {
//                    currentPair = parentIterator.next();
//                    currentValue = currentPair[MIN];
//                }
//                return currentValue;
//            }
//        };
//    }
//
//    @Override
//    boolean contains(int i) {
//        return collection.stream().anyMatch(pair->(i >= pair[MIN] && i <=pair[MAX]));
//    }
//}
