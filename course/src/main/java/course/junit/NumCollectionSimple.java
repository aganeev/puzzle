package course.junit;

import java.util.*;

public class NumCollectionSimple implements Iterable<Integer>{
    private Set<Integer> collection;
    private Integer lastMax;


    public NumCollectionSimple(String numCollectionString) throws UnsortedValuesException {
        collection = new HashSet<>();
        String[] separatedByComma = numCollectionString.split(",");
        for (String value : separatedByComma) {
            value = value.trim();
            int dashIndex = value.indexOf('-', 1);
            if (dashIndex >= 0) {
                handleInterval(value, dashIndex);
            } else {
                handleSingleValue(value);
            }
        }
    }

    private void handleSingleValue(String value) throws UnsortedValuesException {
        int intValue = Integer.parseInt(value);
        if (lastMax != null && intValue < lastMax) {
            throw new UnsortedValuesException();
        }
        collection.add(intValue);
        lastMax = intValue;
    }


    private void handleInterval(String value, int dashIndex) throws UnsortedValuesException {
        int firstInt = Integer.parseInt(value.substring(0, dashIndex));
        int secondInt = Integer.parseInt(value.substring(dashIndex + 1));
        if (firstInt > secondInt || (lastMax != null && lastMax > firstInt)) {
            throw new UnsortedValuesException();
        }
        for (int i = firstInt; i <= secondInt; i++) {
            collection.add(i);
        }
        lastMax = secondInt;
    }


    public boolean contains(int number){
        return collection.contains(number);
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }
}
