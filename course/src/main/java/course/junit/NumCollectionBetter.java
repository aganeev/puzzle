package course.junit;

import java.util.Iterator;

public class NumCollectionBetter implements Iterable<Integer>{
    private boolean[] collection;
    private int smallest;

    private static final int MIN = 0;
    private static final int MAX = 1;


    public NumCollectionBetter(String numCollectionString) throws UnsortedValuesException {
        String[] separatedByComma = numCollectionString.split(",");
        int largestMin = smallest = parseValues(separatedByComma[0])[MIN];
        int largest = parseValues(separatedByComma[separatedByComma.length - 1])[MAX];
        if (smallest > largest) {
            throw new UnsortedValuesException();
        }
        collection = new boolean[largest - smallest + 1];
        for (String aSeparatedByComma : separatedByComma) {
            int[] parsedValues = parseValues(aSeparatedByComma);
            if (parsedValues[MIN] < largestMin || parsedValues[MAX] > largest) {
                throw new UnsortedValuesException();
            }
            for (int j = parsedValues[MIN] - smallest; j <= parsedValues[MAX] - smallest; j++) {
                collection[j] = true;
            }
            largestMin = parsedValues[MAX];
        }
    }

    private int[] parseValues(String value) throws UnsortedValuesException {
        int[] returnValue = new int[2];
        value = value.trim();
        int dashIndex = value.indexOf('-', 1);
        if (dashIndex >= 0) {
            int firstInt = Integer.parseInt(value.substring(0, dashIndex));
            int secondInt = Integer.parseInt(value.substring(dashIndex + 1));
            if (firstInt > secondInt) {
                throw new UnsortedValuesException();
            }
            returnValue[MIN] = firstInt;
            returnValue[MAX] = secondInt;
        } else {
            int intValue = Integer.parseInt(value);
            returnValue[MIN] = returnValue[MAX] = intValue;
        }
        return returnValue;
    }

    public boolean contains(int number){
        int index = number - smallest;
        if (index < 0 || index > collection.length - 1) {
            return false;
        }
        return collection[index];
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }
}
