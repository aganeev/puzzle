package course.persons;

import static com.google.common.base.Preconditions.checkNotNull;

public class Person {
    private String name;

    Person(String name) {
        this.name = checkNotNull(name);
    }

    @Override
    public String toString() {
        return getName();
    }

    String getName() {
        return name;
    }
}
