package course.persons;

import static com.google.common.base.Preconditions.checkNotNull;

public class Employee extends Person {

    private Employee manager;

    public Employee(String name) {
        super(name);
    }
    public Employee(String name, Employee manager) {
        super(name);
        setManager(manager);
    }

    public void setManager(Employee manager) {
        this.manager = checkNotNull(manager);
    }

    Employee getManager() {
        return manager;
    }

    @Override
    public String toString() {
        return super.toString() + (manager != null ? ", reporting to: " + manager.getName() : " - I'm the big boss!");
    }

}
