package course.persons;

import java.util.Scanner;

public class EmployeeSearchMachine{
    private Organization organization;
    private static final String QUIT_WORD = "quit";

    public EmployeeSearchMachine(Organization organization) {
        this.organization = organization;
    }

    public void doSearch(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the name that you want to search");
        String employeeForSearch;
        while (!isExitCommand(employeeForSearch = scanner.nextLine())) {
            if (organization.hasEmployee(employeeForSearch))
                organization.printEmployeesOf(employeeForSearch);
            else
                System.out.println("No such employee");
            System.out.println("If you want to repeat the search please enter another name.\nIf you want to stop the search type 'quit' without quotes");
        }
        System.out.println("Exiting...");
    }

    private boolean isExitCommand(String string){
        return QUIT_WORD.toLowerCase().equals(string.toLowerCase());
    }
}
