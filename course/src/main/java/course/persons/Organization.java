package course.persons;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

public class Organization {
    private String organizationName;
    private Map<String,Employee> employees = new HashMap<>();

    public Organization(String organizationName) {
        setName(organizationName);
    }

    public String getName() {
        return organizationName;
    }

    public void setName(String organizationName) {
        this.organizationName = checkNotNull(organizationName);
    }

    /**
     * @param name an Employee organizationName. Employee with such organizationName should present in the organization
     * @return the Employee object with organizationName equals to the specified one
     * @exception NoSuchElementException if an Employee with such name isn't exist in organization
     */
    public Employee getEmployee(String name) {
        if (hasEmployee(name))
            return employees.get(name);
        else
            throw new NoSuchElementException("No such employee with organizationName" + name);
    }

    /**
     * @param name an Employee organizationName that is being checked for existence in this organization
     * @return true if an Employee with such organizationName is present in this organization, otherwise false
     */
    public boolean hasEmployee(String name){
        return employees.containsKey(checkNotNull(name));
    }

    /**
     * @param employee an Employee that should be added to this organization.
     *                 existent Employee with the same will be replaced
     */
    public void addEmployee(Employee employee) {
        checkNotNull(employee);
        employees.put(employee.getName(), employee);
    }

    /**
     * The method looks for top employee(s) (employee(s) that haven't manager) and prints subordination structure from them
     */
    public void printOrganizationStructure(){
        Set<Employee> bigBosses = findEmployeesWithoutManager();
        if (bigBosses.isEmpty())
            System.out.println("The \"" + organizationName + "\" organization has closed submission or no employees. It's impossible to print structure.");
        else
            bigBosses.forEach(boss->printEmployeesOf(boss.getName()));
    }

    /**
     * The method prints subordination structure of employee with specified name
     * @param name an Employee's name.
     */
    public void printEmployeesOf(String name){
        printEmployeesOf(name,"");
    }


    public void printEmployeesOf(String name, String prefix){
        if (hasEmployee(name)) {
            System.out.println(prefix + name);
            String finalPrefix = prefix + "\t";
            employees.values().stream()
                    .filter(employee -> employee.getManager() != null && name.equals(employee.getManager().getName()))
                    .forEach(employee -> printEmployeesOf(employee.getName(), finalPrefix));
        }
    }

    private Set<Employee> findEmployeesWithoutManager() {
        return employees.values().stream().filter(employee -> employee.getManager() == null).collect(Collectors.toSet());
    }
}
