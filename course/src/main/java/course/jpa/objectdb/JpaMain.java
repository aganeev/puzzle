package course.jpa.objectdb;

import course.jpa.hibernate.Person;

import javax.persistence.*;
import java.util.List;

public class JpaMain {
    public static void main(String[] args) {

        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("$objectdb/db/mydb1.odb");

        EntityManager em = emf.createEntityManager();

//        Person person = new Person();
//        person.setBirthday(new Date());
//        person.setName("Artem");
//        person.setId(12345);
//
//        em.getTransaction().begin();
//        em.persist(person);
//        em.getTransaction().commit();


        TypedQuery<Person> query = em.createQuery(
                "SELECT person FROM Person person WHERE person.name = 'Artem'", Person.class);
        List<Person> results = query.getResultList();
        for(Person p : results) {
            em.getTransaction().begin();
            em.remove(p);
            em.getTransaction().commit();
        }

//        em.getTransaction().begin();
//        Query delete = em.createQuery("DELETE FROM Person");
//        delete.executeUpdate();
//        em.getTransaction().commit();

        TypedQuery<Person> query2 = em.createQuery(
                "SELECT person FROM Person person", Person.class);
        List<Person> results2 = query2.getResultList();
        for(Person p : results2) {
            System.out.println(p);
        }

//        Person person2 = em.find(Person.class, 123);

//        System.out.println(person2);


        em.close();

        emf.close();

    }
}
