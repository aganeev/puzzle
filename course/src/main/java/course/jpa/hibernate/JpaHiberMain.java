package course.jpa.hibernate;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class JpaHiberMain {

    private static Person artem;
    private static Person somebody;
    private static Person somebodyElse;

    public static void main(String[] args) {


        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("jpa-example");

        EntityManager em = emf.createEntityManager();

        fillData(em);

        Query cityPersonsQuery = em.createQuery("select p.city.name, p from Person p where p.city.name = :city");
        cityPersonsQuery.setParameter("city", "Tel Aviv");

        List<Object[]> result = cityPersonsQuery.getResultList();
        result.forEach(singleResult-> System.out.println(Arrays.toString(singleResult)));

        System.out.println();

        Query countryPersonsQuery = em.createQuery("select p.city.country.name, p from Person p where p.city.country.name = :country");
        countryPersonsQuery.setParameter("country", "Israel");

        List<Object[]> result2 = countryPersonsQuery.getResultList();
        result2.forEach(singleResult-> System.out.println(Arrays.toString(singleResult)));

        System.out.println();

        TypedQuery<Person> myCityPersonsQuery = em.createQuery("select p from Person p where p.city = :city", Person.class);
        myCityPersonsQuery.setParameter("city", artem.getCity());
        List<Person> result3 = myCityPersonsQuery.getResultList();
        result3.forEach(System.out::println);

        System.out.println();

        myCityPersonsQuery.setParameter("city", somebodyElse.getCity());
        List<Person> result4 = myCityPersonsQuery.getResultList();
        result4.forEach(System.out::println);

        System.out.println();

        TypedQuery<Person> myCountryPersonsQuery = em.createQuery("select p from Person p where p.city.country = :country", Person.class);
        myCountryPersonsQuery.setParameter("country", somebody.getCity().getCountry());
        List<Person> result5 = myCountryPersonsQuery.getResultList();
        result5.forEach(System.out::println);


        em.close();

        emf.close();

    }

    private static void fillData(EntityManager em) {
        em.getTransaction().begin();

        Query delete = em.createQuery("DELETE FROM Person");
        Query delete2 = em.createQuery("DELETE FROM Country");
        Query delete3 = em.createQuery("DELETE FROM City");
        delete.executeUpdate();
        delete3.executeUpdate();
        delete2.executeUpdate();

        em.getTransaction().commit();
        em.getTransaction().begin();

        Country russia = new Country("Russia");
        Country israel = new Country("Israel");
        City moscow = new City("Moscow", russia);
        City kazan = new City("Kazan", russia);
        City telAviv = new City("Tel Aviv", israel);
        City yaffo = new City("Yaffo", israel);

        Person moshe = new Person("Moshe", new Date());
        moshe.setCity(moscow);

        Person david = new Person("David",new Date());
        david.setCity(telAviv);

        Person abram = new Person("Abram",new Date());
        abram.setCity(telAviv);

        artem = new Person("Artem",new Date());
        artem.setCity(kazan);

        somebody = new Person("Somebody",new Date());
        somebody.setCity(yaffo);

        somebodyElse = new Person("Somebody Else",new Date());
        somebodyElse.setCity(moscow);

        moshe.addFriend(david);

        em.persist(moshe);
        em.persist(david);
        em.persist(abram);
        em.persist(artem);
        em.persist(somebody);
        em.persist(somebodyElse);

        em.getTransaction().commit();

    }
}
