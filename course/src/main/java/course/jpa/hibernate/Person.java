package course.jpa.hibernate;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Person {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private Date birthday;
    @OneToMany
    @JoinTable(name="friends")
    private Set<Person> friends;
    @OneToOne(cascade = CascadeType.PERSIST)
    private City city;

    public Person(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void addFriend(Person person) {
        lazyFriendsInit();
        friends.add(person);
    }

    public boolean removeFriend(long id) {
        lazyFriendsInit();
        return friends.removeIf((Person p) -> p.getId() == id);
    }

    public Set<Person> getFriends() {
        return friends;
    }

    private void lazyFriendsInit() {
        if (friends == null) {
            friends = new HashSet<>();
        }
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", friends=" + friends +
                ", city=" + city +
                '}';
    }
}
