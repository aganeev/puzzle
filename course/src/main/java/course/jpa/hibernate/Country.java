package course.jpa.hibernate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Country {
    @Id @GeneratedValue
    private long id;
    private String name;
    @OneToMany(targetEntity=City.class, mappedBy="country", cascade= CascadeType.PERSIST)
    private List<City> cities = new ArrayList<>();

    public Country(String name) {
        this.name = name;
    }

    public Country() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCity(City city) {
        cities.add(city);
    }

    @Override
    public String toString() {
        return name;
    }
}
