package course.jpa.hibernate;

import javax.persistence.*;

@Entity
public class City {
    @Id @GeneratedValue
    private long id;
    private String name;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Country country;

    public City(String name, Country country) {
        this.name = name;
        this.country = country;

    }

    public City() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name + ", " + country;
    }
}
