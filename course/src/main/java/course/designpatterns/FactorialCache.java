package course.designpatterns;

import java.util.*;

public class FactorialCache {
    private TreeMap<Integer,Long> cache; // Use TreeMap instead
    private static final int SIZE = 5;

    public FactorialCache() {
        cache = new TreeMap<>();
    }

    public void set(int key, long value) {
        if (cache.containsKey(key)) {
            return;
        }
        if (cache.size() == SIZE) {
            cache.pollFirstEntry();
        }
        cache.put(key, value);
    }

    public Long get(int key) {
        return cache.get(key);
    }
}
