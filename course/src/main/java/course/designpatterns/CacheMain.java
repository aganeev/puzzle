package course.designpatterns;


import java.util.stream.IntStream;

public class CacheMain {
    private FactorialCache cache;
    public static void main(String[] args) {
        CacheMain cacheMain = new CacheMain();
        IntStream.rangeClosed(1, 3)
                .forEach(i ->{
                    long startTime = System.nanoTime();
                    long value = cacheMain.factorial(3);
                    long estimatedTime = System.nanoTime() - startTime;
                    System.out.println("value: " + value + " time: " + estimatedTime);

                    startTime = System.nanoTime();
                    value = cacheMain.factorial(10);
                    estimatedTime = System.nanoTime() - startTime;
                    System.out.println("value: " + value + " time: " + estimatedTime);

                    startTime = System.nanoTime();
                    value = cacheMain.factorial(20);
                    estimatedTime = System.nanoTime() - startTime;
                    System.out.println("value: " + value + " time: " + estimatedTime);
                });

        cacheMain.factorial(4);
        cacheMain.factorial(5);
        cacheMain.factorial(6);
        cacheMain.factorial(7);
        cacheMain.factorial(8);


        long startTime = System.nanoTime();
        long value = cacheMain.factorial(20);
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("value: " + value + " time: " + estimatedTime);

    }

    public CacheMain() {
        cache = new FactorialCache();
    }

    private long factorial(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("Factorial can be calculated only for natural numbers");
        }
        long calculated = 1;
        for (int i = n; i>=1 ; i--) {
            Long cached = cache.get(i);
            if (cached != null) {
                System.out.println("cache used");
                calculated = cached * calculated;
                break;
            } else {
                calculated = calculated * i;
            }
        }
        cache.set(n, calculated);
        return calculated;
    }

}
