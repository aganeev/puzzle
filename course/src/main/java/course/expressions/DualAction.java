package course.expressions;

abstract class DualAction extends Expression {
    private Expression arg1;
    private Expression arg2;
    private String sign;

    public DualAction(Expression arg1, Expression arg2, String sign) {
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.sign = sign;
    }

    protected void setSign(String sign) {
        this.sign = sign;
    }

    public Expression getArg1() {
        return arg1;
    }

    public Expression getArg2() {
        return arg2;
    }

    @Override
    public String toString() {
        return "(" + arg1.toString() + sign + arg2.toString() + ")";
    }

}
