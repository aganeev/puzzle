package course.expressions;

public class Number extends Expression {
    private Double number;

    public Number(Double number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.valueOf(evaluate());
    }


    @Override
    protected Double evaluateToDouble() {
        return number;
    }
}
