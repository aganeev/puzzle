package course.expressions;

public abstract class Expression {

    public java.lang.Number evaluate(){
        return doubleToNumber(evaluateToDouble());
    }

    protected abstract Double evaluateToDouble();

    private java.lang.Number doubleToNumber(Double number){
        if (number == Math.floor(number))
            return number.intValue();
        return number;
    }
}
