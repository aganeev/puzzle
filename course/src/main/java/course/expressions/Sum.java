package course.expressions;

public class Sum extends DualAction {

    public Sum(Expression arg1, Expression arg2) {
        super(arg1, arg2, "+");
        setSign(getRightSign(arg2.toString()));
    }

    @Override
    protected Double evaluateToDouble() {
        return (getArg1().evaluateToDouble() + getArg2().evaluateToDouble());
    }

    private String getRightSign(String arg){
        if (arg.charAt(0) == '-')
            return "";
        return "+";
    }
}
