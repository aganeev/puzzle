package course.expressions;

public class Exponent extends DualAction {
    public Exponent(Expression arg1, Expression arg2) {
        super(arg1, arg2, "^");
    }

    @Override
    protected Double evaluateToDouble() {
        return Math.pow(getArg1().evaluateToDouble(),getArg2().evaluateToDouble());
    }
}
