package course.multithreading;

public class Thread2 implements Runnable {
    private PrintNumbers printer;

    public Thread2(PrintNumbers printer) {
        this.printer = printer;
    }

    @Override
    public void run() {
        System.out.println("Thread2 ThreadId: " + Thread.currentThread().getId());
        // do our thing
        printer.printNumbers();
    }
}
