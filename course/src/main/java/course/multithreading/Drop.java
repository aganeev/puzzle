package course.multithreading;

import java.util.concurrent.atomic.AtomicReference;

public class Drop {
    private AtomicReference<String> message = new AtomicReference<>("");

    public synchronized String take() {
        while (message.get().isEmpty()) {
             try {
                 wait();
             } catch (InterruptedException e) {}
        }
        String returnValue = message.getAndSet("");
        notifyAll();
        return returnValue;
    }

    public synchronized void put(String newMessage) {
        while (!message.get().isEmpty()) {
             try {
                 wait();
             } catch (InterruptedException e) {}
        }
        message.set(newMessage);
        notifyAll();
    }


}
