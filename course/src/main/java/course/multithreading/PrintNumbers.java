package course.multithreading;

public class PrintNumbers {
    public void printNumbers() {
        for(int i=0; i<1000; i++) {
            synchronized(this) {
                System.out.print(Thread.currentThread().getId());
                System.out.print(": ");
                System.out.println(i);
            }
        }
    }
}
