package course.multithreading;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorMain {
    public static void main(String[] args) throws InterruptedException {
        int printerNumber = 100; //Integer.valueOf(args[0]);
        int threadNumber = 10; //Integer.valueOf(args[1]);
        PrintNumbers printer = new PrintNumbers();

        ArrayBlockingQueue<Runnable> printQueue = new ArrayBlockingQueue<>(100);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(threadNumber,threadNumber,5000,
                TimeUnit.MILLISECONDS,printQueue);

        for (int i = 0; i < printerNumber; i++ ){
            threadPoolExecutor.execute(new Thread2(printer));
        }

        threadPoolExecutor.shutdown();
        do {
            threadPoolExecutor.awaitTermination(2, TimeUnit.SECONDS);
        } while ((!threadPoolExecutor.isTerminated()));
    }

}
