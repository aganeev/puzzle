package course.multithreading.level2;

import java.io.IOException;

public class FileValidatorMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        PuzzleManager puzzleManager = new PuzzleManager();
        puzzleManager.doWork( "src/main/resources", "target/");
    }
}
