package course.multithreading.level2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

class Validator {
    void validate(String inFile, String outFile, AtomicInteger trueCounter) {
        Random random = new Random();
        boolean result = random.nextBoolean();
        Path path = Paths.get(outFile);
        byte[] strToBytes = String.valueOf(result).getBytes();
        try {
            Files.write(path, strToBytes);
        } catch (IOException e) {
            System.out.println("Get IO");
        }
        if (result) {
            trueCounter.incrementAndGet();
        }
    }
}
