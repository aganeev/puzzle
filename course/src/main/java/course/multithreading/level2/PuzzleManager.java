package course.multithreading.level2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

class PuzzleManager {
    void doWork(String inputPath, String outputPath) throws IOException, InterruptedException {
        Path input = Paths.get(inputPath);
        ExecutorService executor = Executors.newFixedThreadPool(4);
        AtomicInteger trueCounter = new AtomicInteger(0);
        try(Stream<Path> paths = Files.walk(input)) {
            paths.forEach(path -> executor.execute(() -> {
                Validator validator = new Validator();
                validator.validate(path.toString(), outputPath + path.getFileName().toString() + "_out",trueCounter);
            }));
        }
        executor.shutdown();
        BooleanSupplier bs = ()->{
            System.out.println("Still working...");
            return false;
        };
        do {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } while ((!executor.isTerminated()) && bs.getAsBoolean());

        System.out.println(trueCounter.toString());
    }
}

