package course.multithreading;

public class WaitNotifyMain {



    public static void main(String[] args) throws InterruptedException {
        Drop drop = new Drop();
        Thread taker = new Thread(new Taker(drop));
        Thread putter = new Thread(new Putter(drop));


        taker.start();
        putter.start();

        taker.join();
        putter.join();

    }


}
