package course.multithreading;

public class Counter {
    private int c;

    synchronized public void increment(){
        c++;
    }

    synchronized public void decrement(){
        c--;
    }

    public int value(){
     return c;
    }
}
