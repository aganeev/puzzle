package course.multithreading.fileindex;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;

import static java.lang.Thread.sleep;

public class ThreadsWithMapMain {
    public static void main(String[] args) throws InterruptedException {
        ArrayList<String> arguments = new ArrayList<>(Arrays.asList(args));
        int numThreads;
        int argIndex = arguments.indexOf("-threads");
        try {
            numThreads = Integer.valueOf(arguments.get(argIndex + 1));
            if (numThreads == -1) {
                throw new IllegalArgumentException("Please specify threads number using '-threads <num_threads>' parameter.");
            }
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Please correct threads number." + ex );
        }
        arguments.remove(argIndex); // removing '-threads'
        arguments.remove(argIndex); // removing value of 'threads' parameter that was shifted left.

        int numFiles = arguments.size();

        ConcurrentMap<String,Map<String,List<Integer>>> index = new ConcurrentSkipListMap<>();

        ExecutorService executor = Executors.newFixedThreadPool(numThreads > numFiles ? numFiles : numThreads);

        arguments.forEach(filePath->{
                File file = new File(filePath);
                executor.execute(new IndexingThread(index, file));
        });

        executor.shutdown();
        index.entrySet().forEach(System.out::println);

    }



}
