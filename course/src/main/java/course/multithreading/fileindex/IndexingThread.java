package course.multithreading.fileindex;

import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentMap;

public class IndexingThread implements Runnable {
    private ConcurrentMap<String,Map<String,List<Integer>>> map;
    private File file;

    IndexingThread(ConcurrentMap<String, Map<String, List<Integer>>> map, File file) {
        this.map = map;
        this.file = file;
    }

    @Override
    public void run() {
        String id = "ThreadId: " + Thread.currentThread().getId();
        int wordIndex = 0;
        try(FileInputStream is = new FileInputStream(file);
            Scanner scanner = new Scanner(is))
        {
            while (scanner.hasNext()) {
                String word = normalizeWord(scanner.next());
                if (!word.isEmpty()) {
                    checkAndUpdate(word, wordIndex++);
                }
            }
        } catch (IOException ex) {
            System.out.println(id + ". Got an exception during file reading " + ex);
        }
        System.out.println("Thread " + id + " have finished file: " + file);
    }

    private String normalizeWord(String word) {
        return StringUtils.strip(word.toLowerCase(),"()!?.,;:[]{}-'\"“”@©=1234567890#$%^&*_");
    }

    private void checkAndUpdate(String word, int position) {
        Map<String, List<Integer>> wordIndex = map.getOrDefault(word, new HashMap<>());
        List<Integer> wordPositions = wordIndex.getOrDefault(file.getName(), new ArrayList<>());
        wordPositions.add(position);
        wordIndex.put(file.getName(), wordPositions);
        map.merge(word,wordIndex,((map1, map2) -> {
            map1.putAll(map2);
            return map1;
        }));
    }

}
