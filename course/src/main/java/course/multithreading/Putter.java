package course.multithreading;

import java.util.Scanner;

public class Putter implements Runnable {
    private Drop drop;

    private static final String QUIT_WORD = "quit";

    public Putter(Drop drop) {
        this.drop = drop;
    }

    @Override
    public void run() {
        try(Scanner scanner = new Scanner(System.in)) {
            String something;
            do {
                System.out.println("Please enter something");
                something = scanner.nextLine();
                drop.put(something);
            } while (!isExitCommand(something));
            System.out.println("Putter " + Thread.currentThread().getId() + " exiting...");
        }
    }

    private static boolean isExitCommand(String string){
        return QUIT_WORD.toLowerCase().equals(string.toLowerCase());
    }
}
