package course.multithreading;

import java.util.ArrayList;

public class AtomicMain {
    static public void main(String[] args) {
        Counter counter = new Counter();
        AtomicCounter atomicCounter = new AtomicCounter();
        System.out.println("Main ThreadId: " + Thread.currentThread().getId());
        ArrayList<Thread> threadArray = new ArrayList<>(2);

        // incrementing threads
        for(int threadCount=0; threadCount<30; ++threadCount) {
            final int count = threadCount;
            Thread thread = new Thread(() -> {
                System.out.println("new ThreadId: " + Thread.currentThread().getId());
                // do our thing
                for (int i = 0; i <= 1000; i++) {
                    if(count < 15) {
                        counter.increment();
                        atomicCounter.increment();
                    }
                    else {
                        counter.decrement();
                        atomicCounter.decrement();
                    }
                }
            }); // don't call run! ...
            threadArray.add(thread);
            thread.start();
        }

        for (int i = 0; i < threadArray.size(); ++i) {
            try {
                threadArray.get(i).join();
            } catch (InterruptedException e) {
                --i;
            }
        }

        System.out.println("Regular counter: " + counter.value());
        System.out.println("Atomic counter: " + atomicCounter.value());
    }
}
