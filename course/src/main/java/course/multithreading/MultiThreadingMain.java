package course.multithreading;


public class MultiThreadingMain {
    public static void main(String[] args) {
        PrintNumbers printer = new PrintNumbers();
        System.out.println("Main ThreadId: " + Thread.currentThread().getId());
        for(int i=0; i<3; i++) {
            new Thread(new Thread2(printer)).start();
            // again, don't call run!
            // (if you want a separate thread)
        }
        printer.printNumbers();
    }

//    static public void main(String[] args) {
//        PrintNumbers printer = new PrintNumbers();
//        System.out.println("Main ThreadId: " + Thread.currentThread().getId());
//        new Thread(() -> {
//            System.out.println("Thread3 ThreadId: " + Thread.currentThread().getId());
//            // do our thing
//            printer.printNumbers();
//        }).start(); // don't call run! ...
//        printer.printNumbers();
//    }
}
