package course.multithreading;

import static java.lang.Thread.sleep;

public class Taker implements Runnable {
    private Drop drop;

    private static final String QUIT_WORD = "quit";

    public Taker(Drop drop) {
        this.drop = drop;
    }

    @Override
    public void run() {
        String something = "";
        while (!something.equals(QUIT_WORD)) {
            something = drop.take();
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(something);
        }
        System.out.println("Taker " + Thread.currentThread().getId() + " exiting...");
    }
}
