package course.puzzle;

import java.util.Objects;

public class PuzzlePlace {
    private Integer top;
    private Integer bottom;
    private Integer left;
    private Integer right;
    private PuzzlePiece puzzlePiece;

    PuzzlePlace() {
        this.top = null;
        this.bottom = null;
        this.left = null;
        this.right = null;
    }


    PuzzlePlace(Integer top, Integer bottom, Integer left, Integer right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    PuzzlePiece getPuzzlePiece() {
        return puzzlePiece;
    }

    void setPuzzlePiece(PuzzlePiece puzzlePiece) {
        this.puzzlePiece = puzzlePiece;
    }

    PuzzlePiece pollPuzzlePiece() {
        PuzzlePiece puzzlePieceToReturn = puzzlePiece;
        this.puzzlePiece = null;
        return puzzlePieceToReturn;
    }

    Integer getTop() {
        return top;
    }

    Integer getBottom() {
        return bottom;
    }

    Integer getLeft() {
        return left;
    }

    Integer getRight() {
        return right;
    }

    void setTop(Integer top) {
        this.top = top;
    }

    void setLeft(Integer left) {
        this.left = left;
    }

    boolean isMatch(PuzzlePiece puzzlePiece) {
        return (left == null || left + puzzlePiece.getLeft() == 0) &&
                (right == null || right + puzzlePiece.getRight() == 0) &&
                (top == null || top + puzzlePiece.getTop() == 0) &&
                (bottom == null || bottom + puzzlePiece.getBottom() == 0);
    }

    @Override
    public String toString() {
        return String.format("Place: left=%s,\ttop=%s,\tright=%s,\tbottom=%s.\t%s", emptyIfNull(left), emptyIfNull(top), emptyIfNull(right), emptyIfNull(bottom), puzzlePiece);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PuzzlePlace)) return false;
        PuzzlePlace that = (PuzzlePlace) o;
        return Objects.equals(getTop(), that.getTop()) &&
                Objects.equals(getBottom(), that.getBottom()) &&
                Objects.equals(getLeft(), that.getLeft()) &&
                Objects.equals(getRight(), that.getRight()) &&
                Objects.equals(getPuzzlePiece(), that.getPuzzlePiece());
    }

    private String emptyIfNull(Integer i) {
        return i == null ? "" : String.valueOf(i);
    }
}
