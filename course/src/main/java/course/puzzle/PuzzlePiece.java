package course.puzzle;

import java.util.Random;

public class PuzzlePiece {
    private int id = 1;
    private int top;
    private int bottom;
    private int left;
    private int right;

    PuzzlePiece(int left, int top, int right, int bottom) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    PuzzlePiece(PuzzlePlace matchedPlace) {
        this.top = matchOrRandom(matchedPlace.getTop());
        this.bottom = matchOrRandom(matchedPlace.getBottom());
        this.left = matchOrRandom(matchedPlace.getLeft());
        this.right = matchOrRandom(matchedPlace.getRight());
    }

    PuzzlePiece(int id, PuzzlePlace matchedPlace) {
        this.id = id;
        this.top = matchOrRandom(matchedPlace.getTop());
        this.bottom = matchOrRandom(matchedPlace.getBottom());
        this.left = matchOrRandom(matchedPlace.getLeft());
        this.right = matchOrRandom(matchedPlace.getRight());
    }

    int getTop() {
        return top;
    }

    int getBottom() {
        return bottom;
    }

    int getLeft() {
        return left;
    }

    int getRight() {
        return right;
    }

    @Override
    public String toString() {
        return ""+ id + " " + left + " " + top + " " + right + " " + bottom;
    }

    private int matchOrRandom(Integer side) {
        if (side == null) {

//            int [] bin = {1,-1,1,-1,1,-1,0,0}; // Normal
//            int [] bin = {1,-1,1,1,-1,1,-1,1,1,-1,0}; // Easy
            int [] bin = {1,-1,0}; // Hard
//            int [] bin = {1,0}; // HardCore


            return bin[(int)(Math.random()*bin.length)];
        } else {
            return 0 - side;
        }

    }
}
