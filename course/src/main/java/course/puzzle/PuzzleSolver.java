package course.puzzle;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class PuzzleSolver {
    private PuzzleBoard board;
    private List<PuzzlePiece> puzzles;

    PuzzleSolver(PuzzleBoard board, List<PuzzlePiece> puzzles) {
        this.board = board;
        this.puzzles = puzzles;
    }

    void solve(){
        long startTime = System.nanoTime();
        if (collectPuzzle(board, puzzles)) {
            System.out.println("\nSucceed\n");
        } else {
            System.out.println("\nFailed\n");
        }
        long elapsedNanos = System.nanoTime() - startTime;
        board.print();
        System.out.println("\nElapsed time: " + TimeUnit.NANOSECONDS.toSeconds(elapsedNanos) + " seconds (" + TimeUnit.NANOSECONDS.toMinutes(elapsedNanos) + " minutes)");
    }

    private boolean collectPuzzle(PuzzleBoard puzzleBoard, List<PuzzlePiece> puzzlePieces) {
        if (puzzlePieces.isEmpty() && puzzleBoard.isLastSet())
            return true;
        List<PuzzlePiece> notMatchedPuzzlePieces = new LinkedList<>();
        while (!puzzlePieces.isEmpty()) {
            PuzzlePiece puzzlePiece = ((LinkedList<PuzzlePiece>) puzzlePieces).poll();
            if (puzzleBoard.currentPlaceIsMatch(puzzlePiece)) {
                puzzleBoard.setPuzzlePiece(puzzlePiece);
                List<PuzzlePiece> restPuzzles = new LinkedList<>(puzzlePieces);
                restPuzzles.addAll(notMatchedPuzzlePieces);
                if (collectPuzzle(puzzleBoard, restPuzzles)) {
                    return true;
                } else {
                    notMatchedPuzzlePieces.add(puzzleBoard.pollPuzzlePiece());
                }
            } else {
                notMatchedPuzzlePieces.add(puzzlePiece);
            }
        }
        puzzlePieces.addAll(notMatchedPuzzlePieces);
        return false;
    }
}
