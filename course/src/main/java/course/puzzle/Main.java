package course.puzzle;

import java.util.*;

public class Main {

    public static void main(String args[]) {
        int[] size = new int[]{8,10}; //parseArgs(args);
        PuzzleBoard puzzleBoard = new PuzzleBoard(size[0], size[1]);
        List<PuzzlePiece> puzzlePieces = puzzleBoard.generateMatchedRandomPieces();
        puzzlePieces.forEach(piece -> System.out.println(piece.toString()));
        // TODO: Debug solver. Something doesn't work there
//        PuzzleSolver solver = new PuzzleSolver(puzzleBoard, puzzlePieces);
//        solver.solve();
    }



    private static int[] parseArgs(String[] args) {
        if (args.length != 2) {
            String error = "Exactly two integer numbers are expected";
            throw new IllegalArgumentException(error);
        }
        int[] result = new int[2];
        for (int i = 0, argsLength = args.length; i < argsLength; i++) {
            String arg = args[i];
            int intValue;
            try {
                intValue = Integer.parseInt(arg);
            } catch (NumberFormatException e) {
                String error = arg + " is wrong value. Integer value is expected";
                throw new IllegalArgumentException(error);
            }
            result[i] = intValue;
        }
        return result;
    }


}
