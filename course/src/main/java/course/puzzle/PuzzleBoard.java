package course.puzzle;

import java.util.*;

class PuzzleBoard {
    private PuzzlePlace[][] board;
    private int rowsNumber;
    private int columnsNumber;
    private int rowIndex;
    private int columnIndex;
    private static final int MAX_SIZE_LIMIT = 30;

    PuzzleBoard(int rowsNumber, int columnsNumber) {
        validateValue(rowsNumber);
        validateValue(columnsNumber);
        this.board = new PuzzlePlace[rowsNumber][columnsNumber];
        this.rowsNumber = rowsNumber;
        this.columnsNumber = columnsNumber;
        initBoard();
    }

    private void validateValue(int value) {
        if (value <= 0) {
            String error = "Size should be more than 0";
            throw new IllegalArgumentException(error);
        }
        if (value > MAX_SIZE_LIMIT) {
            String error = "Size should be less than " + MAX_SIZE_LIMIT;
            throw new IllegalArgumentException(error);
        }
    }

    boolean currentPlaceIsMatch(PuzzlePiece puzzlePiece) {
        return getCurrentPlace().isMatch(puzzlePiece);
    }

    void setPuzzlePiece(PuzzlePiece puzzlePiece) {
        getCurrentPlace().setPuzzlePiece(puzzlePiece);
        if (hasDown())
            getDown().setTop(puzzlePiece.getBottom());
        if (hasRight())
            getRight().setLeft(puzzlePiece.getRight());
        movePointerToNext();
    }

    PuzzlePiece pollPuzzlePiece() {
        PuzzlePiece puzzlePieceToReturn = getCurrentPlace().pollPuzzlePiece();
        movePinterToPreviouse();
        if (hasDown())
            getDown().setTop(null);
        if (hasRight())
            getRight().setLeft(null);
        return puzzlePieceToReturn;
    }


    private boolean hasNext() {
        return !((rowIndex >= rowsNumber - 1) && (columnIndex >= columnsNumber - 1));
    }

    boolean isLastSet() {
        return board[rowsNumber - 1][columnsNumber - 1].getPuzzlePiece() != null;
    }

    private void movePointerToNext() {
        if (columnIndex >= columnsNumber - 1 && hasNext()) {
            rowIndex++;
            columnIndex = 0;
        } else if (columnIndex < columnsNumber - 1) {
            columnIndex++;
        }
    }

    private void movePinterToPreviouse() {
        if (columnIndex == 0 && rowIndex != 0) {
            rowIndex--;
            columnIndex = columnsNumber - 1;
        } else if (columnIndex != 0) {
            columnIndex--;
        }
    }

    private PuzzlePlace getCurrentPlace(){
        return board[rowIndex][columnIndex];
    }

    private PuzzlePlace getRight(){
        return board[rowIndex][columnIndex + 1];
    }

    private boolean hasRight() {
        return (columnIndex < columnsNumber - 1);
    }

    private PuzzlePlace getDown(){
        return board[rowIndex + 1][columnIndex];
    }

    private boolean hasDown() {
        return (rowIndex < rowsNumber - 1);
    }

    private void initBoard() {
        for (int i = 0; i < rowsNumber; i++){
            for (int j = 0; j < columnsNumber; j++){
                Integer top = null;
                Integer bottom = null;
                Integer left = null;
                Integer right = null;
                if (i == 0) {
                    top = 0;
                } else if (i == rowsNumber - 1) {
                    bottom = 0;
                }
                if (j == 0) {
                    left = 0;
                } else if (j == columnsNumber - 1) {
                    right = 0;
                }
                board[i][j] = new PuzzlePlace(top, bottom, left, right);
            }
        }
        rowIndex = columnIndex = 0;
    }

    void print() {
        for (int i = 0; i < rowsNumber; i++){
            System.out.print("| ");
            for (int j = 0; j < columnsNumber; j++){
                System.out.print(board[i][j].toString());
                System.out.print(" \t| ");
            }
            System.out.print("\n");
        }
    }

    List<PuzzlePiece> generateMatchedRandomPieces() {
        List<PuzzlePiece> puzzlePieces = new LinkedList<>();
        int id = rowsNumber*columnsNumber;
        List<Integer> ids = new ArrayList<>(id);
        for (int i = 1; i <= id; i++) {
            ids.add(i);
        }
        Collections.shuffle(ids);
        Iterator<Integer> idIterator = ids.iterator();
        do {
            PuzzlePiece puzzlePiece = new PuzzlePiece(idIterator.next(), getCurrentPlace());
            puzzlePieces.add(puzzlePiece);
            setPuzzlePiece(puzzlePiece);
        } while (!isLastSet());
        Collections.shuffle(puzzlePieces);
        initBoard();
        return puzzlePieces;
    }

}
