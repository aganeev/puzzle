package course.lessons;

import course.network2.Test;

import java.util.Map;
import java.util.TreeMap;

public class Lesson1 {

    private static void printCharReport(Map<Character, Integer> charsMap) {
        charsMap.forEach((key, value) -> System.out.println(key + " : " + intToStars(value)));
    }

    private static Map<Character, Integer> countChars(String...args){
        Map<Character, Integer> charsMap = new TreeMap<>();
        for (String arg : args) {
            arg.chars()
                    .mapToObj(i -> (char)i)
                    .forEach(charr -> {
                        Character caseInsensitiveChar = Character.toLowerCase(charr);
                        int value = charsMap.getOrDefault(caseInsensitiveChar, 0);
                        charsMap.put(caseInsensitiveChar, ++value);
                    });
        }
        return charsMap;
    }

    // SDK 11 is required for calling String.repeat() method!!!
//    private static String intToStars(int i) {
//        if (i >= 0)
//            return "*".repeat(i);
//        return "";
//    }

    private static String intToStars(int n) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            builder.append("*");
        }
        return builder.toString();
    }
}
