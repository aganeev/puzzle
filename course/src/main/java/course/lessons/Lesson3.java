package course.lessons;

import course.iterablestring.NewString;
import course.persons.EmployeeSearchMachine;
import course.persons.Organization;
import course.persons.Employee;

public class Lesson3 {
    public static void main(String[] args) {
        exerciseCompany(args);

//        exerciseIterableString(args);
    }

    private static void exerciseCompany(String[] args) {
        Employee nir = new Employee("Nir");
        Employee nataly = new Employee("Nataly", nir);
        Employee yael = new Employee("Yael", nataly);
        Employee alex = new Employee("Alex", yael);
        Employee artem = new Employee("Artem", alex);
        Employee sagiv = new Employee("Sagiv", nataly);
        Employee eran = new Employee("Eran", sagiv);
        Employee lior = new Employee("Lior", alex);
        Employee nir2 = new Employee("Another Nir");
        Employee amir = new Employee("Amir", nir2);

        Organization myOrganization = new Organization("ATT");
        myOrganization.addEmployee(nir);
        myOrganization.addEmployee(nataly);
        myOrganization.addEmployee(yael);
        myOrganization.addEmployee(alex);
        myOrganization.addEmployee(artem);
        myOrganization.addEmployee(sagiv);
        myOrganization.addEmployee(eran);
        myOrganization.addEmployee(lior);
        myOrganization.addEmployee(amir);
        myOrganization.addEmployee(nir2);

        System.out.println("\nprintOrganizationStructure method example");
        myOrganization.printOrganizationStructure();
        System.out.println("\n");

        EmployeeSearchMachine search = new EmployeeSearchMachine(myOrganization);
        search.doSearch();
    }









    private static void exerciseIterableString(String[] args) {
        NewString myString = new NewString("ajsefhakejhf");
        System.out.println(myString);
        for (char c : myString) {
            System.out.println(c);
        }
    }

}
