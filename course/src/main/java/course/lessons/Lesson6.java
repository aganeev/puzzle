package course.lessons;


public class Lesson6 {
    public static void main(String[] args) {
        System.out.println(factorial(1));
        System.out.println(factorial(2));
        System.out.println(factorial(4));
        System.out.println(factorial(20));


//        System.out.println(fibonacciForEach(1));
//        System.out.println(fibonacciForEach(2));
//        System.out.println(fibonacciForEach(4));
//        System.out.println(fibonacciForEach(5));
//        System.out.println(fibonacciForEach(6));
//        System.out.println(fibonacciForEach(7));
    }



    private static long factorial(int n){
        if (n < 1)
            throw new IllegalArgumentException("Factorial can be calculated only for natural numbers");
        if (n == 1)
            return n;
        return n*factorial(n-1);
    }

    private static long factorialWithLoop(int n){
        if (n < 1)
            throw new IllegalArgumentException("Factorial can be calculated only for natural numbers");
        long result = 1;
        for (int i = 1; i<=n ; i++) {
            result = result*i;
        }
        return result;
    }

    private static long fibonacci(int n){
        return fibonacciRec(n)[1];
    }


    private static long[] fibonacciRec(int n){
        if (n < 1)
            throw new IllegalArgumentException("Fibonacci can be calculated only for natural numbers");
        long[] forReturn = new long[2];
        if (n == 1) {
            forReturn[0] = 0;
            forReturn[1] = 1;
            return forReturn;
        }
        if (n == 2) {
            forReturn[0] = 1;
            forReturn[1] = 1;
            return forReturn;
        }
        long[] result = fibonacciRec(n-1);
        forReturn[0] = result[1];
        forReturn[1] = result[0] + result[1];
        return forReturn;
    }

    private static long fibonacciForEach(int n){
        long currentResult = 0;
        long previousResult = 0;
        for (int i = 1; i <= n; i++) {
            if (i == 1) {
                currentResult = 1;
                previousResult = 0;
            } else if (i == 2) {
                previousResult = currentResult;
            } else {
                long tempPreviousResult = currentResult;
                currentResult = previousResult + currentResult;
                previousResult = tempPreviousResult;
            }
        }
        return currentResult;
    }

    // TODO Factorial with loop

}
