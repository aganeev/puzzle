package course.lessons;

import course.rectangles.Game;

public class Lesson2 {
    public static void main(String[] args) {
        Game game = new Game(80,25);
        game.play();
    }
}
