package course.stack;

public class Main {
    public static void main(String[] args) {
        BetterStack myStack = new BetterStack(19);
        try {
            myStack.pop();
        }
        catch(ArrayIndexOutOfBoundsException e) {
            System.out.println("pop OOPS: " + e);
        }
        for (int i = 0; i < 10; i++) {
            try {
                myStack.push(i + 1);
            }
            catch(ArrayIndexOutOfBoundsException e) {
                System.out.println("push OOPS: " + e);
            }
            System.out.println(myStack.getSum());
        }
        System.out.println("\n");
        for (int member : myStack) {
            System.out.println(member);
        }
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
    }


}
