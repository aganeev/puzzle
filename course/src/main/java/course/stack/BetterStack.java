package course.stack;

public class BetterStack extends MyStack {
    private int sum = 0;

    public BetterStack(int size) {
        super(size);
    }

    @Override
    public void push(int member){
        super.push(member);
        sum +=member;
    }

    @Override
    public int pop(){
        int memberToRemove = super.pop();
        sum -=memberToRemove;
        return memberToRemove;
    }

    public int getSum(){
        return sum;
    }
}
