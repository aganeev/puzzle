package course.stack;

import java.util.Iterator;

public class MyStack implements Iterable<Integer> {
    private int currentIndex = -1;
    private int[] members;

    public MyStack(int size) {
        members = new int[size];
    }

    public void push(int newMember){
        // we do it in 2 lines since the first
        // may throw an exception!
        // in which case we shouldn't increment currentIndex
        members[currentIndex + 1] = newMember;
        ++currentIndex;
    }

    public int pop(){
        int val = members[currentIndex];
        currentIndex--;
        return val;
    }


    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            private int lastIndex = currentIndex;
            @Override
            public boolean hasNext() {
                return lastIndex >= 0;
            }

            @Override
            public Integer next() {
                return members[lastIndex--];
            }
        };
    }
}
