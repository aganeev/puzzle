package course.maze;

public class Maze {
    private char[][] maze;
    private Coordinate start;

    private static final char START_CHAR = 'S';
    private static final char EXIT_CHAR = 'E';
    private static final char PASS_CHAR = ' ';
    private static final char WALL_CHAR = '#';

    public Maze(char[][] array, Coordinate start) {
        maze = array; // TODO check start not null
        this.start = start;
    }

    public void printMaze(){
        System.out.println();
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[0].length; j++) {
                System.out.print(maze[i][j]);
            }
            System.out.println();
        }
    }


    public char getPointChar(Coordinate coordinate) {
        return maze[coordinate.getY()][coordinate.getX()];
    }

    public boolean isOutOfWall(Coordinate coordinate) {
        return coordinate == null ||
                coordinate.getX() >= maze[0].length ||
                coordinate.getX() < 0 ||
                coordinate.getY() >= maze.length ||
                coordinate.getY() < 0;
    }

    public void setTrail(char c, Coordinate coordinate) {
        maze[coordinate.getY()][coordinate.getX()] = c;
    }

    public Coordinate getStart() {
        return start;
    }

    public static char getStartChar() {
        return START_CHAR;
    }

    public static char getExitChar() {
        return EXIT_CHAR;
    }

    public static char getPassChar() {
        return PASS_CHAR;
    }

    public static char getWallChar() {
        return WALL_CHAR;
    }
}
