package course.maze;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class MazeFactory {
    private static final String START_SIGN = "s";
    private static final String END_SIGN = "e";
    private static final String PASS_SIGN = " ";

    private int maxRows = 1;
    private int maxCols;
    private char[][] array;
    private Coordinate start;
    private boolean isFoundExit;


    public Maze getCharMazeFromFile(String source){
        int lineIndex = 0;
        try(InputStream is = new FileInputStream(new File(source));
            Scanner scanner = new Scanner(is)) {
            while (scanner.hasNext() && lineIndex < maxRows + 1) {
                String line = scanner.nextLine();
                if (lineIndex == 0) {
                    initMaze(line);
                } else {
                    setRowToArray(line, lineIndex - 1);
                }
                lineIndex++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!isFoundExit) {
            throw new IllegalArgumentException("Maze should have exit");
        }
        if (start == null) {
            throw new IllegalArgumentException("Maze should have start");
        }
        return new Maze(array, start);
    }

    private void initMaze(String line){
        String[] size = line.split("\\s+");
        try {
            maxRows = Integer.valueOf(size[0]);
            maxCols = Integer.valueOf(size[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Cannot parse maze size: " + e);
        }
        array = new char[maxRows][maxCols];
        for (int i = 0; i < maxRows; i++) {
            Arrays.fill(array[i], Maze.getWallChar());
        }
    }

    private void setRowToArray(String line, int lineIndex) {
        line = normalizeLine(line);
        parseStart(line, lineIndex);
        parseExit(line, lineIndex);
        parsePass(line, lineIndex);
    }

    private void parsePass(String line, int lineIndex) {
        int passIndex = line.indexOf(PASS_SIGN);
        while (passIndex > 0) {
            array[lineIndex][passIndex] = Maze.getPassChar();
            passIndex = line.indexOf(PASS_SIGN, passIndex + 1);
        }
    }

    private void parseExit(String line, int lineIndex) {
        int exitIndex = line.indexOf(END_SIGN);
        if (exitIndex >= 0) {
            if (isFoundExit || line.indexOf(START_SIGN, exitIndex + 1) >= 0) {
                throw new IllegalArgumentException("Maze cannot have more than one exit");
            }
            array[lineIndex][exitIndex] = Maze.getExitChar();
            isFoundExit = true;
        }
    }

    private void parseStart(String line, int lineIndex) {
        int startIndex = line.indexOf(START_SIGN);
        if (startIndex >= 0) {
            if (start != null || line.indexOf(START_SIGN, startIndex + 1) >= 0) {
                throw new IllegalArgumentException("Maze cannot have more than one start");
            }
            array[lineIndex][startIndex] = Maze.getStartChar();
            start = new Coordinate(startIndex, lineIndex);
        }
    }

    private String normalizeLine(String string) {
        if (maxCols > string.length()) {
            System.out.println("There is less columns that was declared so we will fill missed as " + Maze.getWallChar());
        } else {
            string = string.substring(0, maxCols - 1);
        }
        string = string.toLowerCase();
        return string;
    }
}
