//package course.maze;
//
//import java.util.*;
//
//public class Minotaur {
//    private Maze maze;
//    private boolean isReturnToKnownPlace;
//    private ResolveDirection direction;
//    private static final int UP = 0;
//    private static final int RIGHT = 1;
//    private static final int DOWN = 2;
//    private static final int LEFT = 3;
//    private static final char TRAIL_CHAR = '.';
//    private static final char I_WAS_HERE = '@';
//
//    public Minotaur(Maze maze) {
//        this.maze = maze;
//    }
//
//    public void doIt() {
//        /* The Minotaur can resolve a maze by two strategies:
//        1. Always going to the left when it's possible
//        2. Always going to the right when it's possible
//        If method called without specific direction, the Minotaur will choose it randomly.
//        */
//        doIt(ResolveDirection.randomDirection());
//    }
//
//    public void doIt(ResolveDirection direction) {
//        this.direction = direction;
//        Coordinate start = maze.getStart();
//        /* getting coordinates of all neighbor cells to understand where we came from.
//        It's needed to call findWay() method
//         */
//        Iterator<Coordinate> neighbors = getNeighbors(start).iterator();
//        Coordinate entrance; // Means the coordinate out of the maze where we came from
//        do {
//            entrance = neighbors.next();
//        } while (neighbors.hasNext() && !maze.isOutOfWall(entrance));
//
//        if (!maze.isOutOfWall(entrance)) {
//            // It's not logically if start point hasn't connection to the outward
//            System.out.println("I cannot find the start =(");
//        } else if (findWay(entrance, start)) {
//            System.out.println("Solution:");
//            maze.printMaze();
//        } else {
//            System.out.println("I lost =(");
//        }
//    }
//
//    private boolean findWay(Coordinate lastPos, Coordinate currentPos){
//        char currentSign = maze.getPointChar(currentPos);
//        if (currentSign == Maze.getExitChar()) {
//            return true;
//        } else if (currentSign == Maze.getWallChar()) {
//            return false;
//        } else if (currentSign == TRAIL_CHAR) {
//            maze.setTrail(I_WAS_HERE, currentPos);
//            // We will turn back without checking all possible directions until we will return to this point again
//            isReturnToKnownPlace = true;
//            return false;
//        } else {
//            maze.setTrail(TRAIL_CHAR, currentPos);
//            if (!findWayWithTryDirections(lastPos, currentPos)) {
//                maze.setTrail(currentSign, currentPos);
//                return false;
//            } else {
//                return true;
//            }
//        }
//    }
//
//    private boolean findWayWithTryDirections(Coordinate previous, Coordinate currentPos) {
//        /* Generally each turn we have 3 directions to check.
//        We needed last position coordinate to not going there and to start from there with specified direction
//        */
//        Iterator<Coordinate> nextTurnIterator = nextIterator(previous, currentPos);
//        boolean isFound;
//        do {
//            isFound = findWay(currentPos, nextTurnIterator.next());
//            if (maze.getPointChar(currentPos) == I_WAS_HERE) {
//                isReturnToKnownPlace = false;
//                maze.setTrail(TRAIL_CHAR, currentPos);
//            }
//        } while (nextTurnIterator.hasNext() && !isFound && !isReturnToKnownPlace);
//        return isFound;
//    }
//
//    // The feature of this iterator is looping over array until returning to the start point (exclude the start point).
//    private Iterator<Coordinate> nextIterator(Coordinate previousPos, Coordinate currentPos){
//        List<Coordinate> nextSteps = getNeighbors(currentPos);
//        // The index of cell where we came from will be a starting point for iterating other options.
//        int excludeIndex = nextSteps.indexOf(previousPos);
//        return new Iterator<>() {
//            private int currentIndex = excludeIndex;
//            @Override
//            public boolean hasNext() {
//                if (direction.equals(ResolveDirection.RIGHT_HAND)) {
//                    // RIGHT_HAND direction means that we will decrease the index. hasNext is false if next index is equals to that we start with
//                    return (currentIndex - excludeIndex != 1 && currentIndex - excludeIndex != -3);
//                } else {
//                    return (excludeIndex - currentIndex != 1 && excludeIndex - currentIndex != -3);
//                }
//            }
//
//            @Override
//            public Coordinate next() {
//                // RIGHT_HAND direction means that we will decrease the index. If index become '-1' after decreasing we change it to 3
//                if (direction.equals(ResolveDirection.RIGHT_HAND)) {
//                    currentIndex--;
//                    if (currentIndex == -1) {
//                        currentIndex = 3;
//                    }
//                } else
//                    // backwards for LEFT_HAND
//                    {
//                    currentIndex++;
//                    if (currentIndex == 4) {
//                        currentIndex = 0;
//                    }
//                }
//                return nextSteps.get(currentIndex);
//            }
//        };
//    }
//
//    // getting coordinates of all neighbor cells.
//    private List<Coordinate> getNeighbors(Coordinate currentPos){
//        List<Coordinate> neighbors = new ArrayList<>(4);
//        neighbors.add(UP, new Coordinate(currentPos.getX(), currentPos.getY() - 1));
//        neighbors.add(RIGHT, new Coordinate(currentPos.getX() + 1, currentPos.getY()));
//        neighbors.add(DOWN, new Coordinate(currentPos.getX(), currentPos.getY() + 1));
//        neighbors.add(LEFT, new Coordinate(currentPos.getX() - 1, currentPos.getY()));
//        return neighbors;
//    }
//
//    private enum ResolveDirection{
//        LEFT_HAND, RIGHT_HAND;
//
//        private static final List<ResolveDirection> VALUES =
//                List.of(values());
//        private static final int SIZE = VALUES.size();
//        private static final Random RANDOM = new Random();
//
//        public static ResolveDirection randomDirection()  {
//            return VALUES.get(RANDOM.nextInt(SIZE));
//        }
//    }
//}
