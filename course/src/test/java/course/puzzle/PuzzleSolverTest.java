//package course.puzzle;
//
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.mockito.Mockito;
//
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.LinkedList;
//import java.util.List;
//
//public class PuzzleSolverTest {
//    private static PuzzleBoard board;
//    private static List<PuzzlePiece> puzzlePieceList;
//    private static PuzzleSolver solver;
//    private static Method collectPuzzle;
//
//    @BeforeClass
//    public static void beforeAll() throws NoSuchMethodException {
//        board = Mockito.mock(PuzzleBoard.class);
//        puzzlePieceList = new LinkedList<>();
//        solver = new PuzzleSolver(board, puzzlePieceList);
//        collectPuzzle = PuzzleSolver.class.getDeclaredMethod("collectPuzzle", PuzzleBoard.class, List.class);
//        collectPuzzle.setAccessible(true);
//    }
//
//    @Test
//    public void testIfNoPuzzlesLeftAndFieldIsFull() throws InvocationTargetException, IllegalAccessException {
//        Mockito.when(board.isLastSet()).thenReturn(true);
//        Assert.assertTrue("Method should return true if List is empty and last place is filled",
//                (Boolean) collectPuzzle.invoke(solver, board, puzzlePieceList));
//    }
//
//    @Test
//    public void testIfNoPuzzlesLeftButFieldIsNotFull() throws InvocationTargetException, IllegalAccessException {
//        Mockito.when(board.isLastSet()).thenReturn(false);
//        Assert.assertFalse("Method should return false if List is empty but last place is not filled",
//                (Boolean) collectPuzzle.invoke(solver, board, puzzlePieceList));
//    }
//
//    @Test
//    public void testIfPuzzlesLeftButFieldIsFull() throws InvocationTargetException, IllegalAccessException {
//        Mockito.when(board.isLastSet()).thenReturn(true);
//        puzzlePieceList.add(Mockito.mock(PuzzlePiece.class));
//
//        Assert.assertFalse("Method should return false if List is not empty but last place is filled",
//                (Boolean) collectPuzzle.invoke(solver, board, puzzlePieceList));
//    }
//
//
//
//}
