//package course.puzzle;
//
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import java.lang.reflect.Field;
//
//public class PuzzleBoardTest {
//
//    private static PuzzleBoard puzzleBoardTest;
//    private static final int rows = 5;
//    private static final int columns = 7;
//    private static Field board;
//    private static Field columnIndex;
//    private static Field rowIndex;
//
//    @BeforeClass
//    public static void beforeAll() throws NoSuchFieldException {
//        initField();
//        board = puzzleBoardTest.getClass().getDeclaredField("board");
//        columnIndex = puzzleBoardTest.getClass().getDeclaredField("columnIndex");
//        rowIndex = puzzleBoardTest.getClass().getDeclaredField("rowIndex");
//        board.setAccessible(true);
//        columnIndex.setAccessible(true);
//        rowIndex.setAccessible(true);
//    }
//
//    private static void initField() {
//        puzzleBoardTest = new PuzzleBoard(rows, columns);
//    }
//
//
//    @Test
//    public void testInitiation() throws IllegalAccessException {
//        initField();
//        PuzzlePlace[][] puzzlePlaceData = (PuzzlePlace[][]) board.get(puzzleBoardTest);
//
//        Assert.assertEquals("Left-top-Corner element should have '0' as left and top elements and 'null' as right and bottom elements",
//                new PuzzlePlace(0, null, 0, null), puzzlePlaceData[0][0]);
//        Assert.assertEquals("Left-bottom-Corner element should have '0' as left and bottom elements and 'null' as right and top elements",
//                new PuzzlePlace(null, 0, 0, null), puzzlePlaceData[rows - 1][0]);
//        Assert.assertEquals("Right-bottom-Corner element should have '0' as right and bottom elements and 'null' as left and top elements",
//                new PuzzlePlace(null, 0, null, 0), puzzlePlaceData[rows - 1][columns - 1]);
//        Assert.assertEquals("Right-top-Corner element should have '0' as right and top elements and 'null' as left and bottom elements",
//                new PuzzlePlace(0, null, null, 0), puzzlePlaceData[0][columns - 1]);
//
//        Assert.assertEquals("Element in the middle of the left side should have '0' as left element and 'null' as other",
//                new PuzzlePlace(null, null, 0, null), puzzlePlaceData[rows - 2][0]);
//        Assert.assertEquals("Element in the middle of the bottom side should have '0' as bottom element and 'null' as other",
//                new PuzzlePlace(null, 0, null, null), puzzlePlaceData[rows - 1][columns - 3]);
//        Assert.assertEquals("Element in the middle of the right side should have '0' as right element and 'null' as other",
//                new PuzzlePlace(null, null, null, 0), puzzlePlaceData[rows - 2][columns - 1]);
//        Assert.assertEquals("Element in the middle of the top side should have '0' as top element and 'null' as other",
//                new PuzzlePlace(0, null, null, null), puzzlePlaceData[0][columns - 3]);
//
//        Assert.assertEquals("Element in the middle 'null' as all elements",
//                new PuzzlePlace(null, null, null, null), puzzlePlaceData[rows - 2][columns - 3]);
//    }
//
//
//    @Test
//    public void testSetPuzzlePieceInTheMiddle() throws IllegalAccessException {
//        int columnIndexBefore = 2;
//        int rowIndexBefore = 3;
//
//        PuzzleBoardTest.columnIndex.set(puzzleBoardTest, columnIndexBefore);
//        PuzzleBoardTest.rowIndex.set(puzzleBoardTest, rowIndexBefore);
//        PuzzlePiece puzzlePiece = new PuzzlePiece(1,1,-1,1);
//        puzzleBoardTest.setPuzzlePiece(puzzlePiece);
//
//        PuzzlePlace puzzlePlace = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore][columnIndexBefore];
//        PuzzlePlace puzzlePlaceRight = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore][columnIndexBefore + 1];
//        PuzzlePlace puzzlePlaceBottom = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore + 1][columnIndexBefore];
//
//        Assert.assertEquals("puzzlePiece.PuzzlePiece on the current place should be equal to origin puzzlePiece", puzzlePiece, puzzlePlace.getPuzzlePiece());
//        Assert.assertEquals("Left Element at the right place should be equal to puzzlePiece's right element", puzzlePiece.getRight(), puzzlePlaceRight.getLeft().intValue());
//        Assert.assertEquals("top Element at the bottom place should be equal to puzzlePiece's bottom element", puzzlePiece.getBottom(), puzzlePlaceBottom.getTop().intValue());
//        Assert.assertEquals("columnIndex should be more by one than previous", columnIndexBefore + 1, columnIndex.get(puzzleBoardTest));
//        Assert.assertEquals("rowIndex should be the same as previous", rowIndexBefore, rowIndex.get(puzzleBoardTest));
//
//    }
//
//    @Test
//    public void testSetPuzzlePieceLastRow() throws IllegalAccessException {
//        int columnIndexBefore = 2;
//        int rowIndexBefore = rows - 1;
//
//        PuzzleBoardTest.columnIndex.set(puzzleBoardTest, columnIndexBefore);
//        PuzzleBoardTest.rowIndex.set(puzzleBoardTest, rowIndexBefore);
//        PuzzlePiece puzzlePiece = new PuzzlePiece(1,0,-1,1);
//        puzzleBoardTest.setPuzzlePiece(puzzlePiece);
//
//        PuzzlePlace puzzlePlace = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore][columnIndexBefore];
//        PuzzlePlace puzzlePlaceRight = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore][columnIndexBefore + 1];
//
//        Assert.assertEquals("puzzlePiece.PuzzlePiece on the current place should be equal to origin puzzlePiece", puzzlePiece, puzzlePlace.getPuzzlePiece());
//        Assert.assertEquals("Left Element at the right place should be equal to puzzlePiece's right element", puzzlePiece.getRight(), puzzlePlaceRight.getLeft().intValue());
//        Assert.assertEquals("columnIndex should be more by one than previous", columnIndexBefore + 1, columnIndex.get(puzzleBoardTest));
//        Assert.assertEquals("rowIndex should be the same as previous", rowIndexBefore, rowIndex.get(puzzleBoardTest));
//    }
//
//    @Test
//    public void testSetToCurrentPlaceLastColumn() throws IllegalAccessException {
//        int columnIndexBefore = columns - 1;
//        int rowIndexBefore = 3;
//
//        PuzzleBoardTest.columnIndex.set(puzzleBoardTest, columnIndexBefore);
//        PuzzleBoardTest.rowIndex.set(puzzleBoardTest, rowIndexBefore);
//        PuzzlePiece puzzlePiece = new PuzzlePiece(1,1,-1,0);
//        puzzleBoardTest.setPuzzlePiece(puzzlePiece);
//
//        PuzzlePlace puzzlePlace = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore][columnIndexBefore];
//        PuzzlePlace puzzlePlaceBottom = ((PuzzlePlace[][]) board.get(puzzleBoardTest))[rowIndexBefore + 1][columnIndexBefore];
//
//        Assert.assertEquals("puzzlePiece.PuzzlePiece on the current place should be equal to origin puzzlePiece", puzzlePiece, puzzlePlace.getPuzzlePiece());
//        Assert.assertEquals("top Element at the bottom place should be equal to puzzlePiece's bottom element", puzzlePiece.getBottom(), puzzlePlaceBottom.getTop().intValue());
//        Assert.assertEquals("columnIndex should be equal to 0", 0, columnIndex.get(puzzleBoardTest));
//        Assert.assertEquals("rowIndex should be more by 1 than previous", rowIndexBefore + 1, rowIndex.get(puzzleBoardTest));
//    }
//
//
//}
