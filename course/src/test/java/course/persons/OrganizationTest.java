//package course.persons;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.mockito.Mockito;
//
//import java.util.NoSuchElementException;
//
//public class OrganizationTest {
//
//    @Test
//    public void constructorSuccessTest(){
//        String name = "Test name";
//        Organization organization = new Organization(name);
//
//        Assert.assertEquals("getName method should return the name of organization specified in constructor", name, organization.getName());
//    }
//
//    @Test
//    public void setNameSuccessTest(){
//        String oldName = "Old name";
//        String newName = "New name";
//        Organization organization = new Organization(oldName);
//        organization.setName(newName);
//
//        Assert.assertNotEquals("newName and oldName shouldn't be equals", newName, oldName);
//        Assert.assertEquals("getName method should return the name specified in setName method", newName, organization.getName());
//    }
//
//    @Test
//    public void addGetEmployeeSuccessTest(){
//        Employee employee = Mockito.mock(Employee.class);
//        Mockito.when(employee.getName()).thenReturn("Some employee name");
//        Organization organization = new Organization("Some name");
//        organization.addEmployee(employee);
//
//        Assert.assertEquals("getEmployee method should return the employee specified in setEmployee method", employee, organization.getEmployee(employee.getName()));
//    }
//
//    @Test
//    public void addEmployeeWithTheSameSuccessTest(){
//        Employee employee1 = Mockito.mock(Employee.class);
//        Employee employee2 = Mockito.mock(Employee.class);
//        Mockito.when(employee1.getName()).thenReturn("Specific name");
//        Mockito.when(employee2.getName()).thenReturn("Specific name");
//        Organization organization = new Organization("Some name");
//        organization.addEmployee(employee1);
//
//        Assert.assertNotEquals("employee1 and employee2 shouldn't be equals", employee1, employee2);
//        Assert.assertEquals("getEmployee method should return the employee specified in setEmployee method", employee1, organization.getEmployee(employee1.getName()));
//
//        organization.addEmployee(employee2);
//
//        Assert.assertEquals("getEmployee method should return the employee specified in setEmployee method", employee2, organization.getEmployee(employee1.getName()));
//    }
//
//    @Test
//    public void addHasEmployeeTrueTest(){
//        Employee employee = Mockito.mock(Employee.class);
//        Mockito.when(employee.getName()).thenReturn("Some employee name");
//        Organization organization = new Organization("Some name");
//        organization.addEmployee(employee);
//
//        Assert.assertTrue("hasEmployee method should return true", organization.hasEmployee(employee.getName()));
//    }
//
//    @Test
//    public void addHasEmployeeFalseTest(){
//        Employee employee = Mockito.mock(Employee.class);
//        String someName = "some name";
//        String anotherName = "another name";
//        Mockito.when(employee.getName()).thenReturn(someName);
//        Organization organization = new Organization("Some organization name");
//        organization.addEmployee(employee);
//
//        Assert.assertNotEquals("someName and anotherName shouldn't be equals", someName, anotherName);
//        Assert.assertFalse("hasEmployee method should return false", organization.hasEmployee(anotherName));
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void printEmployeesOfNullTest(){
//        Organization organization = new Organization("Some name");
//        organization.printEmployeesOf(null);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void hasEmployeeNullTest(){
//        Organization organization = new Organization("Some name");
//        organization.hasEmployee(null);
//    }
//
//    @Test(expected = NoSuchElementException.class)
//    public void getEmployeeNoSuchEmployeeTest(){
//        Organization organization = new Organization("Some name");
//        organization.getEmployee("Some employee name");
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void getEmployeeNullTest(){
//        Organization organization = new Organization("Some name");
//        organization.getEmployee(null);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void addEmployeeNullTest(){
//        Organization organization = new Organization("Some name");
//        organization.addEmployee(null);
//    }
//
//
//    @Test(expected = NullPointerException.class)
//    public void constructorNameNullFailedTest(){
//        new Organization(null);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void setNameNullFailedTest(){
//        Organization organization = new Organization("Some name");
//        organization.setName(null);
//    }
//
//
//
//
//}
