//package course.persons;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.mockito.Mockito;
//
//public class EmployeeTest {
//
//    @Test
//    public void constructorOneParamSuccessTest(){
//        String name = "TestName";
//        Employee employee = new Employee(name);
//
//        Assert.assertEquals("Constructor with only name parameter should create an Employee with the specified name", name, employee.getName());
//        Assert.assertNull("Constructor with only name parameter should create an Employee without manager", employee.getManager());
//    }
//
//    @Test
//    public void constructorTwoParamsSuccessTest(){
//        String name = "TestName2";
//        Employee manager = Mockito.mock(Employee.class);
//        Employee employee = new Employee(name, manager);
//
//        Assert.assertEquals("Constructor with only name parameter should create an Employee with the specified name", name, employee.getName());
//        Assert.assertEquals("Constructor with only name parameter should create an Employee with the specified manager", manager, employee.getManager());
//    }
//
//    @Test
//    public void setManagerSuccessTest(){
//        String name = "TestName3";
//        Employee manager = Mockito.mock(Employee.class);
//        Employee employee = new Employee(name);
//        employee.setManager(manager);
//
//        Assert.assertEquals("getManager method should return the same object that was set by setManager method", manager, employee.getManager());
//    }
//
//    @Test
//    public void toStringManagerNotNullTest(){
//        String name = "employeeName";
//        Employee manager = Mockito.mock(Employee.class);
//        Mockito.when(manager.getName()).thenReturn("managerName");
//        Employee employee = new Employee(name, manager);
//
//        Assert.assertEquals(name + ", reporting to: " + manager.getName(), employee.toString());
//    }
//
//    @Test
//    public void toStringManagerNullTest(){
//        String name = "employeeName";
//        Employee employee = new Employee(name);
//
//        Assert.assertEquals(name + " - I'm the big boss!", employee.toString());
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void setManagerNullFailedTest(){
//        Employee employee = new Employee("Test Name5");
//        employee.setManager(null);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void constructorOneParamNullFailedTest(){
//        new Employee(null);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void constructorTwoParamsNameNullFailedTest(){
//        Employee manager = Mockito.mock(Employee.class);
//        new Employee(null, manager);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void constructorTwoParamsManagerNullFailedTest(){
//        new Employee("Test name", null);
//    }
//
//
//
//
//}
