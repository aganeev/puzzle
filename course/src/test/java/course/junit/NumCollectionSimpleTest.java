//package course.junit;
//
//import org.junit.Test;
//
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//public class NumCollectionSimpleTest {
//
//    @Test
//    public void createWithSingleZeroValueSuccess() throws UnsortedValuesException {
//        String input = "0";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for integer value of specified in constructor string",
//                collection.contains(Integer.valueOf(input)));
//    }
//
//    @Test
//    public void createWithPluralSortedPositiveValuesSuccess() throws UnsortedValuesException {
//        String input = "1, 3, 10";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(1));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(3));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(10));
//    }
//
//    @Test
//    public void createWithPluralSortedNegativeValuesSuccess() throws UnsortedValuesException {
//        String input = "-8, -5, -1";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-8));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-5));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-1));
//    }
//
//    @Test
//    public void createWithPluralSortedPositiveAndNegativeValuesSuccess() throws UnsortedValuesException {
//        String input = "-8, -5, 0, 4, 40";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-8));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-5));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(0));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(4));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(40));
//    }
//
//    @Test
//    public void createWithSinglePositiveIntervalValuesSuccess() throws UnsortedValuesException {
//        String input = "1-5";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(1));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(3));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(5));
//    }
//
//    @Test
//    public void createWithSingleNegativeIntervalValuesSuccess() throws UnsortedValuesException {
//        String input = "-50--10";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-50));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-15));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-10));
//    }
//
//    @Test
//    public void createWithSinglePositiveAndNegativeIntervalValuesSuccess() throws UnsortedValuesException {
//        String input = "-50-10";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-50));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(0));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(10));
//    }
//
//    @Test
//    public void createWithSameValuesAsIntervalSuccess() throws UnsortedValuesException {
//        String input = "1-1";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(1));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(-1));
//    }
//
//    @Test
//    public void createWithManyIntervalsAndSingleValuesSuccess() throws UnsortedValuesException {
//        String input = "-50--10, -5, -3--1, 0, 2, 15, 20-30, 35, 100-101";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-50));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-15));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-5));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(-2));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(0));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(2));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(15));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(20));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(21));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(30));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(35));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(100));
//        assertTrue("contains() method should return true for specified in ctor value", collection.contains(101));
//    }
//
//    @Test
//    public void containsValuesBetweenSingleNumbersFalse() throws UnsortedValuesException {
//        String input = "1,3";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(2));
//    }
//
//    @Test
//    public void containsValuesOutsideOfIntervalFalse() throws UnsortedValuesException {
//        String input = "1-3";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(0));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(4));
//    }
//
//    @Test
//    public void containsValuesOutsideOfManySpecifiedInputsFalse() throws UnsortedValuesException {
//        String input = "-20--15, -10, 1-3, 6, 105";
//        NumCollectionSimple collection = new NumCollectionSimple(input);
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(-21));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(-14));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(-9));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(0));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(10));
//        assertFalse("contains() method should return false for not specified in ctor value", collection.contains(5));
//    }
//
//    @Test(expected = UnsortedValuesException.class)
//    public void createWithPluralUnsortedPositiveValuesFailed() throws UnsortedValuesException {
//        String input = "3,2";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = UnsortedValuesException.class)
//    public void createWithIntervalsIntersectionFailed() throws UnsortedValuesException {
//        String input = "1-10,5-15";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = UnsortedValuesException.class)
//    public void createWithSingleAndIntervalUnsortedPositiveValuesFailed() throws UnsortedValuesException {
//        String input = "5-9, 1";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = UnsortedValuesException.class)
//    public void createWithSingleAndIntervalUnsortedNegativeValuesFailed() throws UnsortedValuesException {
//        String input = "-9, -20--15";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = UnsortedValuesException.class)
//    public void createWithSingleUnsortedIntervalFailed() throws UnsortedValuesException {
//        String input = "-1--30";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = NumberFormatException.class)
//    public void createWithNotParsableAsIntValuesFailed() throws UnsortedValuesException {
//        String input = "1, 1.4";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = NumberFormatException.class)
//    public void createWithNotParsableStringValuesFailed() throws UnsortedValuesException {
//        String input = "a";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = NumberFormatException.class)
//    public void createWithEmptyStringFailed() throws UnsortedValuesException {
//        String input = "";
//        new NumCollectionSimple(input);
//    }
//
//    @Test(expected = NullPointerException.class)
//    public void createWithNullFailed() throws UnsortedValuesException {
//        new NumCollectionSimple(null);
//    }
//}
