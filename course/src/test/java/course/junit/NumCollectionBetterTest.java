//package course.junit;
//
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//
//import java.util.stream.Stream;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class NumCollectionBetterTest {
//
//    private static Stream<Arguments> createTestSuccessData() {
//        return Stream.of(
//                Arguments.of("0",                               new int[]{0},                               "Single zero value"),
//                Arguments.of("1, 3, 10",                        new int[]{1,3,10},                          "Several sorted positive single values"),
//                Arguments.of("-8, -5, -1",                      new int[]{-8,-5,-1},                        "Several sorted negative single values"),
//                Arguments.of("-8, -5, 0, 4, 40",                new int[]{-8,-5,0,4,40},                    "Several sorted positive and negative single values"),
//                Arguments.of("1-5",                             new int[]{1,3,5},                           "Positive interval value"),
//                Arguments.of("-50--10",                         new int[]{-50,-10,-15},                     "Negative interval value"),
//                Arguments.of("-50-10",                          new int[]{-50,0,10},                        "TransZero interval value"),
//                Arguments.of("1-1",                             new int[]{1},                               "The same value as interval"),
//                Arguments.of("-50--10, -5, 0, 15, 20-30, 35",   new int[]{-50,-25,-10,-5,0,15,20,25,30,35}, "Many single and interval values")
//        );
//    }
//
//    private static Stream<Arguments> containsTestFalseData() {
//        return Stream.of(
//                Arguments.of("1,3",                         new int[]{2},                                       "Value between single numbers"),
//                Arguments.of("1-3",                         new int[]{4,0},                                     "Values outside interval of numbers"),
//                Arguments.of("-20--15, -10, 1-3, 6, 105",   new int[]{-21,-14,-11,-9,-1,0,4,5,7,10,5,100},      "Not specified values")
//        );
//    }
//
//    private static Stream<Arguments> createFailedData() {
//        return Stream.of(
//                Arguments.of("3,1",         UnsortedValuesException.class,  "Unsorted two single values"),
//                Arguments.of("1,2,0,17,19", UnsortedValuesException.class,  "Unsorted several single values"),
//                Arguments.of("3-1",         UnsortedValuesException.class,  "Unsorted positive interval"),
//                Arguments.of("1-10, 5-15",  UnsortedValuesException.class,  "Interval intersection"),
//                Arguments.of("-10--30",     UnsortedValuesException.class,  "Unsorted negative interval"),
//                Arguments.of("5-9, 1",      UnsortedValuesException.class,  "Unsorted interval and single values"),
//                Arguments.of("-9, -20--15", UnsortedValuesException.class,  "Unsorted interval and single values"),
//                Arguments.of("1, 1.4",      NumberFormatException.class,    "Not integer value"),
//                Arguments.of("a",           NumberFormatException.class,    "String value"),
//                Arguments.of("",            NumberFormatException.class,    "Empty string value"),
//                Arguments.of("1, 3-5-7",    NumberFormatException.class,    "Invalid interval"),
//                Arguments.of(null,          NullPointerException.class,     "Null value")
//        );
//    }
//
//    @ParameterizedTest(name = "{2}")
//    @MethodSource("createTestSuccessData")
//    void createAndContainsSuccess(String input, int[] dataToCheck, String caseName) throws UnsortedValuesException {
//        NumCollectionBetter collection = new NumCollectionBetter(input);
//        for (int i : dataToCheck) {
//            assertTrue(collection.contains(i), "contains() method should return 'true' for value=" + i);
//        }
//    }
//
//    @ParameterizedTest(name = "{2}")
//    @MethodSource("containsTestFalseData")
//    void containsFalse(String input, int[] dataToCheck, String caseName) throws UnsortedValuesException {
//        NumCollectionBetter collection = new NumCollectionBetter(input);
//        for (int i : dataToCheck) {
//            assertFalse(collection.contains(i), "contains() method should return 'false' for value=" + i);
//        }
//    }
//
//    @ParameterizedTest(name = "{2}")
//    @MethodSource("createFailedData")
//    void containsFalse(String input, Class<Exception> expectedException, String caseName) {
//        assertThrows(expectedException, () -> new NumCollectionBetter(input));
//    }
//}
