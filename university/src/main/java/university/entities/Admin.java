package university.entities;


import javax.persistence.Entity;

@Entity
public class Admin extends User {

    public Admin() {
    }

    Admin(String login, String password) {
        super(login, password);
    }
}
