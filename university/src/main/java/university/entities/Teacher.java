package university.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Teacher extends User {
    @ManyToMany
    @JoinTable(name="teacher_student",
            joinColumns = @JoinColumn(
                    name = "teacher",
                    referencedColumnName = "login"),
            inverseJoinColumns = @JoinColumn(
                    name = "student",
                    referencedColumnName = "login"))
    private Set<Student> students;

    Teacher(String login, String password) {
        super(login, password);
        students = new HashSet<>();
    }

    public Teacher() {
    }

    public Set<Student> getStudents() {
        return students;
    }

    void addStudent(Student student) {
        students.add(student);
    }

    @Override
    public String toString() {
        return "Teacher{ full name: " + getFullName() + " }";
    }
}
