package university.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Student extends User {
    @ManyToMany(cascade = CascadeType.PERSIST, targetEntity=Teacher.class, mappedBy="students")
    private Set<Teacher> teachers;

    Student(String login, String password) {
        super(login, password);
        teachers = new HashSet<>();
    }

    public Student() {
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
        teacher.addStudent(this);
    }

    @Override
    public String toString() {
        return "Student{ full name: " + getFullName() + " }";
    }
}
