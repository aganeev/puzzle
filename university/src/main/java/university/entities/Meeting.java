package university.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Meeting {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private LocalDateTime date;
    private int duration;
    @ManyToOne
    private Teacher teacher;
    @OneToOne
    private Student student;

    public Meeting() {
    }

    public Meeting(Teacher teacher, LocalDateTime date, int duration) {
        this.date = date;
        this.duration = duration;
        this.teacher = teacher;
    }

    public long getId() {
        return id;
    }

    public void register(Student student) {
        this.student = student;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getDuration() {
        return duration;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public Student getStudent() {
        return student;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "date=" + date +
                ", duration=" + duration +
                ", teacher=" + teacher +
                ", student=" + student +
                '}';
    }
}
