package university.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class User {
    @Id
    private String login;
    private String fullName;
    private String password;

    User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static User newUser(String login, String password, Role role) {
        switch (role) {
            case TEACHER:
                return new Teacher(login, password);
            case STUDENT:
                return new Student(login, password);
            case ADMIN:
                return new Admin(login, password);
            default:
                return null;
        }
    }

    public String toStringForAdmin() {
        return this.getClass().getSimpleName() +
                " { login: " + login + ", full name: " + fullName + " }";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getLogin(), user.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogin());
    }
}
