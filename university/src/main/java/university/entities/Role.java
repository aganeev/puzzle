package university.entities;

public enum Role {
    STUDENT, TEACHER, ADMIN
}
