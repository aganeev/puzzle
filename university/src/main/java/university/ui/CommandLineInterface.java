package university.ui;

import com.sun.istack.internal.Nullable;
import org.apache.commons.lang3.StringUtils;
import university.menu.UserMenu;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class CommandLineInterface implements UserInterface {
    private Scanner scanner;
    private Class<? extends UserMenu.Commands> commands;

    public CommandLineInterface() {
        scanner = new Scanner(System.in);
    }

    @Override @Nullable
    public String getUserCredentials() {
        StringBuilder credentials = new StringBuilder();
        System.out.println("Username:");
        String userName = scanner.nextLine();
        if (StringUtils.isEmpty(userName)) {
            return null;
        } else {
            credentials.append(userName);
        }
        credentials.append(":");
        System.out.println("Password:");
        String password = scanner.nextLine();
        if (password == null) {
            return null;
        } else {
            credentials.append(password);
        }
        return credentials.toString();
    }

    @Override
    public void show(String string) {
        System.out.println(string);
    }

    @Override @Nullable
    public String getTextInput() {
        String input = scanner.nextLine();
        if (input == null) {
            return null;
        } else {
            return input;
        }
    }

    @Override
    public void setMenu(Class<? extends UserMenu.Commands> commands) {
        this.commands = commands;
        showMenu();
    }

    @Override
    public UserMenu.Commands getMenuChoice() {
        UserMenu.Commands retVal = null;
        String input = scanner.nextLine();
        int inputNumber = parseInt(input);
        retVal = valueOf(inputNumber);
        if (retVal == null) {
            retVal = valueOf(99);
        }
        return retVal;
    }

    @Override
    public void showMenu() {
        System.out.println("Please choose:");
        for (UserMenu.Commands enumVal : commands.getEnumConstants()) {
            System.out.println(enumVal.getNumber() + " " + enumVal.getMessage());
        }
    }

    private UserMenu.Commands valueOf(Integer inputNumber) {
        UserMenu.Commands retVal = null;
        for (UserMenu.Commands enumVal : commands.getEnumConstants()) {
            if (inputNumber.equals(enumVal.getNumber())) {
                retVal = enumVal;
            }
        }
        return retVal;
    }

    private int parseInt(String input) {
        if (input == null) {
            return 0;
        }
        if (input.matches("\\d+")) {
            return Integer.valueOf(input);
        } else {
            return -1;
        }
    }

    @Override
    public void showAsList(Collection objects) {
        objects.forEach(System.out::println);
    }

    @Override
    public LocalDateTime getCalendarInput() {
        LocalDateTime date = null;
        while (date == null) {
            show("Please provide date/time in next format: \"2007-12-03T10:15:30\"");
            String input = getTextInput();
            if (input == null) return null;
            try {
                date = LocalDateTime.parse(input);
            } catch (DateTimeParseException e) {
                show("Wrong input");
            }
        }
        return date;
    }

    @Override
    public int choose(List<?> options) {
        IntStream.rangeClosed(1, options.size())
                .forEach(idx -> show(idx + " " + options.get(idx - 1)));
        return parseInt(getTextInput()) - 1;
    }
}
