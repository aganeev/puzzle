package university.ui;

import university.menu.UserMenu;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface UserInterface {
    String getUserCredentials();

    void show(String string);

    String getTextInput();

    void setMenu(Class<? extends UserMenu.Commands> commands);

    UserMenu.Commands getMenuChoice();

    void showMenu();

    void showAsList(Collection students);

    LocalDateTime getCalendarInput();

    int choose(List<?> options);
}
