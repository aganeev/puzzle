package university.menu;

import university.entities.Meeting;
import university.entities.Student;
import university.entities.Teacher;
import university.ui.UserInterface;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class StudentMenu implements UserMenu {
    private Student user;
    private EntityManager entityManager;
    private UserInterface userInterface;

    public StudentMenu(Student user, EntityManager entityManager, UserInterface userInterface) {
        this.user = user;
        this.entityManager = entityManager;
        this.userInterface = userInterface;
    }

    @Override
    public void start() {
        userInterface.setMenu(Commands.class);
        Commands userChoice;
        while (!(userChoice = (Commands) userInterface.getMenuChoice()).equals(Commands.EXIT)) {
            handleUserChoice(userChoice);
        }
    }

    private void handleUserChoice(Commands userChoice) {
        switch (userChoice) {
            case SHOW_MY_TEACHERS:
                showTeachers();
                break;
            case ADD_TEACHER:
                addTeacher();
                break;
            case SHOW_MY_MEETINGS:
                showMeetings();
                break;
            case REGISTER_TO_MEETING:
                registerToMeeting();
                break;
            case MENU:
                break;
            case EXIT:
                userInterface.show("Bye-bye!");
                return;
        }
        userInterface.showMenu();
    }

    private void registerToMeeting() {
        if (user.getTeachers().isEmpty()) {
            userInterface.show("You haven't teachers. Choose teacher first");
            return;
        }
        TypedQuery<Meeting> meetingsQuery = entityManager.createQuery(
                "select m from Meeting m where m.teacher in :studentTeachers and m.student is null", Meeting.class);
        meetingsQuery.setParameter("studentTeachers", user.getTeachers());
        List<Meeting> meetings = meetingsQuery.getResultList();
        if (meetings.isEmpty()) {
            userInterface.show("Your teachers haven't meetings.");
            return;
        }
        int choice = userInterface.choose(meetings);
        if (choice >= 0 && choice < meetings.size()) {
            Meeting meetingToAdd = meetings.get(choice);
            entityManager.getTransaction().begin();
            meetingToAdd.register(user);
            entityManager.getTransaction().commit();
            userInterface.show("Success!");
        } else {
            userInterface.show("Bad choice :(");
        }
    }


    private void showMeetings() {
        TypedQuery<Meeting> myMeetingsQuery = entityManager.createQuery(
                "select m from Meeting m where m.student = :student", Meeting.class);
        myMeetingsQuery.setParameter("student", user);
        List<Meeting> meetings = myMeetingsQuery.getResultList();
        if (meetings.isEmpty()) {
            userInterface.show("You haven't meetings. Please register to them");
            return;
        }
        userInterface.showAsList(meetings);
    }

    private void addTeacher() {
        TypedQuery<Teacher> teachersQuery = entityManager.createQuery(
                "select t from Teacher t where :student not member of t.students", Teacher.class);
        teachersQuery.setParameter("student", user);
        List<Teacher> teachers = teachersQuery.getResultList();
        int choice = userInterface.choose(teachers);
        if (choice >= 0 && choice < teachers.size()) {
            Teacher teacherToAdd = teachers.get(choice);
            entityManager.getTransaction().begin();
            user.addTeacher(teacherToAdd);
            entityManager.getTransaction().commit();
            userInterface.show("Success!");
        } else {
            userInterface.show("Bad choice :(");
        }
    }

    private void showTeachers() {
        if (user.getTeachers().isEmpty()) {
            userInterface.show("You haven't teachers. Choose teacher first");
            return;
        }
        userInterface.showAsList(user.getTeachers());
    }

    public enum Commands implements UserMenu.Commands{
        SHOW_MY_TEACHERS(1,"Show my teachers"),
        ADD_TEACHER(2,"Add teacher"),
        SHOW_MY_MEETINGS(3, "Show my meetings"),
        REGISTER_TO_MEETING(4, "Register to new meeting"),
        EXIT(0,"Exit"),
        MENU(99,"Repeat this menu");

        private int number;
        private String message;

        Commands(int number, String message) {
            this.number = number;
            this.message = message;
        }

        public int getNumber() {
            return number;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }
}
