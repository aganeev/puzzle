package university.menu;

import university.entities.Meeting;
import university.entities.Teacher;
import university.ui.UserInterface;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;

public class TeacherMenu implements UserMenu {
    private Teacher user;
    private EntityManager entityManager;
    private UserInterface userInterface;

    TeacherMenu(Teacher user, EntityManager entityManager, UserInterface userInterface) {
        this.user = user;
        this.entityManager = entityManager;
        this.userInterface = userInterface;
    }

    @Override
    public void start() {
        userInterface.setMenu(Commands.class);
        Commands userChoice;
        while (!(userChoice = (Commands) userInterface.getMenuChoice()).equals(Commands.EXIT)) {
            handleUserChoice(userChoice);
        }
    }

    private void handleUserChoice(Commands userChoice) {
        switch (userChoice) {
            case SHOW_STUDENTS:
                showStudents();
                break;
            case SHOW_MEETING:
                showMeetings();
                break;
            case ADD_MEETING:
                addMeeting();
                break;
            case MENU:
                break;
            case EXIT:
                userInterface.show("Bye-bye!");
                return;
        }
        userInterface.showMenu();
    }

    private void addMeeting() {
        userInterface.show("Please provide a new Date:");
        LocalDateTime date = userInterface.getCalendarInput();
        if (date == null) return;
        userInterface.show("Please provide meeting duration:");
        String userInput = userInterface.getTextInput();
        if (userInput == null) return;
        while (getValidDuration(userInput) == -1) {
            userInterface.show("Wrong duration. Try again");
            userInput = userInterface.getTextInput();
            if (userInput == null) return;
        }
        int duration = Integer.parseInt(userInput);
        entityManager.getTransaction().begin();
        Meeting newMeeting = new Meeting(user, date, duration);
        entityManager.persist(newMeeting);
        entityManager.getTransaction().commit();
    }

    private int getValidDuration(String input) {
        if (input.matches("\\d+")) {
            int value = Integer.parseInt(input);
            if (value > 0 && value < 24) {
                return value;
            }
        }
        return -1;
    }

    private void showMeetings() {
        TypedQuery<Meeting> meetingsQuery = entityManager.createQuery("select m from Meeting m where m.teacher = :teacher", Meeting.class);
        meetingsQuery.setParameter("teacher", user);
        List<Meeting> myMeetings = meetingsQuery.getResultList();
        if (myMeetings.isEmpty()) {
            userInterface.show("You haven't meetings. Please create them");
            return;
        }
        userInterface.showAsList(myMeetings);
    }

    private void showStudents() {
        if (user.getStudents().isEmpty()) {
            userInterface.show("You haven't students");
            return;
        }
        userInterface.showAsList(user.getStudents());
    }

    public enum Commands implements UserMenu.Commands {
        SHOW_STUDENTS(1,"Show my students"),
        SHOW_MEETING(2,"Show my meetings"),
        ADD_MEETING(3,"Add new meeting"),
        EXIT(0,"Exit"),
        MENU(99,"Repeat this menu");

        private int number;
        private String message;

        Commands(int number, String message) {
            this.number = number;
            this.message = message;
        }

        public int getNumber() {
            return number;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

}
