package university.menu;

import university.entities.Admin;
import university.entities.Student;
import university.entities.Teacher;
import university.entities.User;
import university.ui.UserInterface;

import javax.persistence.EntityManager;

public interface UserMenu {

    static UserMenu getMenu(User user, EntityManager entityManager, UserInterface anInterface) {
        if (user instanceof Student) {
            return new StudentMenu((Student) user, entityManager, anInterface);
        } else if (user instanceof Teacher) {
            return new TeacherMenu((Teacher) user, entityManager, anInterface);
        } else if (user instanceof Admin) {
            return new AdminMenu(user, entityManager, anInterface);
        } else {
            return null;
        }
    }

    void start();

    // Interface for inner ENUM
    interface Commands {
        int getNumber();
        String getMessage();
    }


}
