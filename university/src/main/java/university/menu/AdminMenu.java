package university.menu;

import university.entities.Role;
import university.entities.User;
import university.ui.UserInterface;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class AdminMenu implements UserMenu {
    private User user;
    private EntityManager entityManager;
    private UserInterface userInterface;

    AdminMenu(User user, EntityManager entityManager, UserInterface userInterface) {
        this.user = user;
        this.entityManager = entityManager;
        this.userInterface = userInterface;
    }

    @Override
    public void start() {
        userInterface.setMenu(Commands.class);
        Commands userChoice;
        while (!(userChoice = (Commands) userInterface.getMenuChoice()).equals(Commands.EXIT)) {
            handleUserChoice(userChoice);
        }
    }

    private void handleUserChoice(Commands userChoice) {
        switch (userChoice) {
            case SHOW_USERS:
                showUsers();
                break;
            case ADD_USER:
                addUser();
                break;
            case DELETE_USER:
                deleteUser();
                break;
            case MENU:
                break;
            case EXIT:
                userInterface.show("Bye-bye!");
                return;
        }
        userInterface.showMenu();
    }

    private void deleteUser() {
        userInterface.show("Please provide username that you want to delete");
        String userToDelete = userInterface.getTextInput();
        if (userToDelete == null) return;
        if (user.getLogin().equals(userToDelete)) {
            userInterface.show("You cannot delete yourself.");
            return;
        }
        entityManager.getTransaction().begin();
        Query delete = entityManager.createQuery("DELETE FROM User user WHERE user.login = '" + userToDelete + "'");
        delete.executeUpdate();
        entityManager.getTransaction().commit();
        userInterface.show("User was deleted even if he wasn't exist");
    }

    private void addUser() {
        userInterface.show("Please provide new username");
        String userName = userInterface.getTextInput();
        if (userName == null) return;
        if (userName.isEmpty()) {
            userInterface.show("Wrong input");
            return;
        }
        User existentUser = entityManager.find(User.class, userName);
        if (existentUser != null) {
            userInterface.show("User already exist: " + existentUser);
            return;
        }
        userInterface.show("Please provide password for " + userName);
        String password = userInterface.getTextInput();
        if (password == null) return;

        while (password.isEmpty()) {
            userInterface.show("Password cannot be empty. Please retype.");
            password = userInterface.getTextInput();
            if (password == null) return;
        }
        userInterface.show("Please choose a role:");
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        roles.add(Role.TEACHER);
        roles.add(Role.ADMIN);
        int choice = userInterface.choose(roles);
        if (choice >= 0 && choice < roles.size()) {
            Role role = roles.get(choice);
            User newUser = User.newUser(userName, password, role);
            entityManager.getTransaction().begin();
            entityManager.persist(newUser);
            entityManager.getTransaction().commit();
            User createdUser = entityManager.find(User.class, userName);
            userInterface.show("Success: " + createdUser.toStringForAdmin());
        } else {
            userInterface.show("Wrong choice :(");
        }

    }

    private void showUsers() {
        TypedQuery<User> query = entityManager.createQuery(
                "SELECT user FROM User user", User.class);
        List<User> users = query.getResultList();
        StringBuilder userList = new StringBuilder("Users:\n");
        for(User dbUser : users) {
            userList.append(dbUser.toStringForAdmin()).append("\n");
        }
        userInterface.show(userList.toString());
    }

    public enum Commands implements UserMenu.Commands {
        SHOW_USERS(1,"Show users"),
        ADD_USER(2,"Add new user"),
        DELETE_USER(3,"Delete User"),
        EXIT(0,"Exit"),
        MENU(99,"Repeat this menu");

        private int number;
        private String message;

        Commands(int number, String message) {
            this.number = number;
            this.message = message;
        }

        public int getNumber() {
            return number;
        }

        public String getMessage() {
            return message;
        }
    }


}
