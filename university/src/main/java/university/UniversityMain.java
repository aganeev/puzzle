package university;


import org.apache.commons.lang3.StringUtils;
import org.hibernate.AnnotationException;
import org.hibernate.MappingException;
import university.entities.Role;
import university.entities.User;
import university.menu.UserMenu;
import university.ui.CommandLineInterface;
import university.ui.UserInterface;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UniversityMain {
    private EntityManager entityManager;
    EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        UniversityMain university = new UniversityMain();
        university.start();
    }

    private void start() {
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory("university");
            entityManager = entityManagerFactory.createEntityManager();

            createDefaultAdminUser();

            UserInterface userInterface = new CommandLineInterface();
            User user = getLoginedUser(userInterface);
            if (user == null) {
                System.exit(1);
            }
            UserMenu userMenu = UserMenu.getMenu(user, entityManager, userInterface);
            if (userMenu == null) {
                System.exit(2);
            }
            userMenu.start();

        } catch (AnnotationException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    private void createDefaultAdminUser() {
        User existentAdmin = entityManager.find(User.class, "Admin");
        if (existentAdmin == null) {
            User newUser = User.newUser("Admin", "Aa123456", Role.ADMIN);
            entityManager.getTransaction().begin();
            entityManager.persist(newUser);
            entityManager.getTransaction().commit();
        }
    }

    private User getLoginedUser(UserInterface userInterface) {
        String userCredentials = userInterface.getUserCredentials();
        if (userCredentials == null) {
            return null;
        }
        String[] userData = userCredentials.split(":");
        String userName = userData[0].trim();
        String userPassword = userData[1].trim();
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(userPassword)) {
            System.err.println("Bad input");
            return null;
        }
        User user = entityManager.find(User.class, userName);
        if (user == null) {
            System.err.println("No such user");
            return null;
        }
        if (!StringUtils.equals(user.getPassword(), userPassword)) {
            System.err.println("Wrong password");
            return null;
        }
        checkFilledName(user, userInterface);
        return user;
    }

    private void checkFilledName(User user, UserInterface userInterface) {
        if (user.getFullName() == null) {
            userInterface.show("Your name is empty. Please provide your full name");
            String fullName = userInterface.getTextInput();
            if (fullName == null) {
                System.exit(2);
            }
            while (fullName.isEmpty()) {
                userInterface.show("Your name cannot be empty. Please provide your full name");
                fullName = userInterface.getTextInput();
                if (fullName == null) System.exit(2);
            }
            entityManager.getTransaction().begin();
            user.setFullName(fullName);
            entityManager.getTransaction().commit();
        }
    }
}
