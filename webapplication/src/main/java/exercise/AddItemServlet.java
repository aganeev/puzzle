package exercise;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/addItem")
public class AddItemServlet extends HttpServlet {
    public AddItemServlet() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String itemName = request.getParameter("name");
        String priceString = request.getParameter("price");

        if (!priceString.matches("\\d+")) {
            try {
                getServletContext().getRequestDispatcher("/wrongPriceFormat").forward(request,response);
            } catch (ServletException e) {
                // print to log
            }
        }

        int price = Integer.parseInt(priceString);
        Map<String,Integer> items = getOrCreateItems();
        items.put(itemName, price);
        doGet(request, response);


    }

    private Map<String, Integer> getOrCreateItems() {
        Map<String, Integer> items = (Map<String, Integer>) getServletContext().getAttribute("items");
        if (items == null) {
            items = new ConcurrentHashMap<>();
        }
        getServletContext().setAttribute("items", items);
        return items;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Add to the list</title></head>");
        out.println(
                "<body><h1>Items</h1>" +
                        "<form action=addItem method=POST>" +
                        "<label for=\"name\">Entity name: </label>" +
                        "<input type=\"text\" name=\"name\">" +
                        "<label for=\"name\">Entity price: </label>" +
                        "<input type=\"text\" name=\"price\">" +
                        "<input type=\"submit\">" +
                        "</form><br><br>" +
                        "<a href=\"priceList\">See all list</a></body></html>"

        );
    }
}
