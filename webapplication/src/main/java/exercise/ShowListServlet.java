package exercise;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet("/priceList")
public class ShowListServlet extends HttpServlet {
    public ShowListServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        Map<String, Integer> items = (Map<String, Integer>) getServletContext().getAttribute("items");
        out.println("<html><head><title>Item list</title></head>");
        out.println("<body><h1>Items</h1>");
        items.forEach((item,price) -> {
            out.println("<p>" + item + ": " + price + "</p>");
        });
        out.println("<br><br><a href=\"addItem\">Add new</a></body></html></body></html>");
    }
}
